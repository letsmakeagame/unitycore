//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;

using blacktriangles;

namespace blacktriangles.Network
{
	public static class JsonActionRegistry
	{
		// members ////////////////////////////////////////////////////////////
		private static BaseRegistry<string, JsonAction, JsonActionAttribute> registry = null;

		// constructor / initializer //////////////////////////////////////////
		public static void Initialize()
		{
			if( registry == null )
			{
				registry = new BaseRegistry<string, JsonAction, JsonActionAttribute>();
				registry.Initialize( (attrib)=>{ return attrib.name; } );
			}
		}

		public static JsonAction Create( JsonObject json )
		{
			Initialize();
			string action = json.GetField<string>( "action" );
			JsonAction result = registry.Instantiate( action );
      result.FromJson( json );
			return result;
		}
	}
}
