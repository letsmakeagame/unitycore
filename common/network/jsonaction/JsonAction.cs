//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Network
{
	public abstract class JsonAction
		: IJsonSerializable
	{
		// members ////////////////////////////////////////////////////////////
		public string action									                      { get { return actionAttribute.name; } }
		public JsonActionAttribute actionAttribute                  { get { return GetJsonActionAttribute(); } }
		private JsonActionAttribute _actionAttribute                = null;

		// constructor / initializer //////////////////////////////////////////
		public JsonAction()
		{
		}

		// public methods /////////////////////////////////////////////////////
		public virtual JsonObject ToJson()
		{
			JsonObject result = new JsonObject();
			result["action"] = action;
			return result;
		}

		public virtual void FromJson( JsonObject json )
		{
		}

		public virtual ActionErrorCode Validate()
		{
			return ActionErrorCode.NONE;
		}

		// private methods ////////////////////////////////////////////////////
        private JsonActionAttribute GetJsonActionAttribute()
		{
			if( _actionAttribute == null )
			{
				_actionAttribute = AssemblyUtility.GetAttribute<JsonActionAttribute>( GetType() );
			}

			return _actionAttribute;
		}
	}
}
