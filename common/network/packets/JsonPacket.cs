//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Network
{
	[Packet(121)]
	public class JsonPacket
		: UTF8TextPacket
	{
		// members ////////////////////////////////////////////////////////////
		public JsonObject json									{ get; protected set; }

		// constructor / initialzier //////////////////////////////////////////
		public JsonPacket()
			: base()
		{
			json = null;
		}

		public JsonPacket( JsonObject _json )
			: base( _json.ToString() )
		{
			json = _json;
		}

    public JsonPacket( IJsonSerializable obj )
      : this( obj.ToJson() )
    {
    }

		// public methods /////////////////////////////////////////////////////
		public override void Decode( RawPacket packet )
		{
			base.Decode( packet );
			json = JsonObject.FromString( text );
		}

		public override RawPacket Encode()
		{
			text = json.ToString();
			return base.Encode();
		}
	}
}
