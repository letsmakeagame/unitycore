//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Network
{
	public class ActionRouter
	{
		// types //////////////////////////////////////////////////////////////
		private class CustomRoute
		{
			public System.Delegate converter					= null;
 			public System.Delegate handler						= null;

			public CustomRoute( System.Delegate _converter, System.Delegate _handler )
			{
				converter = _converter;
				handler = _handler;
			}
		}

		// members ////////////////////////////////////////////////////////////
		private List<System.Func<BaseConnection,JsonObject,bool>> prehandlers = null;
		private List<CustomRoute> customRoutes					= null;
		private Dictionary<string, System.Delegate> handlers	= null;
		private Dictionary<string, System.Action<BaseConnection,JsonObject>> rawhandlers = null;

		// constructor / initializer //////////////////////////////////////////
		public ActionRouter()
		{
			prehandlers = new List<System.Func<BaseConnection,JsonObject,bool>>();
			customRoutes = new List<CustomRoute>();
			handlers = new Dictionary<string,System.Delegate>();
			rawhandlers = new Dictionary<string,System.Action<BaseConnection,JsonObject>>();
		}

		// public methods /////////////////////////////////////////////////////
		public bool Route( BaseConnection connection, JsonObject payload )
		{
			bool result = false;

			// route prehandlers //
			foreach( var prehandler in prehandlers )
			{
				bool res = prehandler( connection, payload );
				if( res )
        {
					return true;
        }
			}

			// router custom handlers //
			foreach( CustomRoute route in customRoutes )
			{
				object res = route.converter.DynamicInvoke( new object[] { payload } );
				if( res != null )
				{
					bool handled = (bool)route.handler.DynamicInvoke( new object[] { connection, res } );
					if( handled )
          {
						return true;
          }
				}
			}

			// route raw handlers first //
			string actionStr = payload.GetField<string>("action");
			if( System.String.IsNullOrEmpty( actionStr ) == false )
			{
				System.Action<BaseConnection,JsonObject> handler = null;
				if( rawhandlers.TryGetValue( actionStr, out handler ) )
				{
					handler( connection, payload );
				}
			}

			// next try action specific handlers //
			JsonAction action = JsonActionRegistry.Create( payload );
			if( action != null )
			{
				System.Delegate handler = null;
				if( handlers.TryGetValue( action.action, out handler ) )
				{
					result = true;
					if( handler != null )
					{
						handler.DynamicInvoke( connection, action );
					}
				}
			}

			return result;
		}

		public void AddPreRoute( System.Func<BaseConnection,JsonObject,bool> handler )
		{
			prehandlers.Add( handler );
		}

		public void AddCustomRoute<ObjectType>( System.Func<JsonObject,ObjectType> converter, System.Func<BaseConnection,ObjectType,bool> handler )
		{
			CustomRoute newroute = new CustomRoute( converter, handler );
			customRoutes.Add( newroute );
		}

		public void AddRoute( string action, System.Action<BaseConnection,JsonObject> handler )
		{
			System.Action<BaseConnection,JsonObject> existing = null;
			if( rawhandlers.TryGetValue( action, out existing ) )
			{
				existing -= handler;
				existing += handler;
			}
			else
			{
				existing = handler;
			}

			rawhandlers[ action ] = handler;
		}

		public bool AddRoute<ActionType>( System.Action<BaseConnection,ActionType> handler )
			where ActionType: JsonAction
		{
			bool didAdd = false;
			JsonActionAttribute attrib = AssemblyUtility.GetAttribute<JsonActionAttribute>( typeof(ActionType) );
			if( attrib != null )
			{
				string action = attrib.name;
				System.Delegate result = null;
				if( handlers.TryGetValue( action, out result ) )
				{
					System.Action<BaseConnection,ActionType> castHandlers = (System.Action<BaseConnection,ActionType>)result;
					castHandlers -= handler;
					castHandlers += handler;
					result = (System.Delegate)castHandlers;
				}
				else
				{
					result = (System.Delegate)handler;
				}

				handlers[action] = result;
				didAdd = true;
			}

			return didAdd;
		}

		public void RemovePreRoute( System.Func<BaseConnection,JsonObject,bool> handler )
		{
			prehandlers.Remove( handler );
		}

		public void RemoveCustomRoute<ObjectType>( System.Func<JsonObject,ObjectType> converter, System.Action<BaseConnection,ObjectType> handler )
		{
			customRoutes.RemoveAll( (route)=>{ return route.converter == converter && route.handler == handler; } );
		}

		public void RemoveRoute( string action, System.Action<BaseConnection,JsonObject> handler )
		{
			System.Action<BaseConnection,JsonObject> existing = null;
			if( rawhandlers.TryGetValue( action, out existing ) )
			{
				existing -= handler;
			}

			rawhandlers[action] = existing;
		}

		public bool RemoveRoute<ActionType>( System.Action<BaseConnection,ActionType> handler )
			where ActionType: JsonAction
		{
			bool didRemove = false;
			JsonActionAttribute attrib = AssemblyUtility.GetAttribute<JsonActionAttribute>( typeof(ActionType) );
			if( attrib != null )
			{
				string action = attrib.name;
				System.Delegate result = null;
				if( handlers.TryGetValue( action, out result ) )
				{
					System.Action<BaseConnection,ActionType> castHandlers = (System.Action<BaseConnection,ActionType>)result;
					castHandlers -= handler;
					result = (System.Delegate)castHandlers;
					didRemove = true;

					handlers[action] = result;
				}
			}

			return didRemove;
		}
    }
}
