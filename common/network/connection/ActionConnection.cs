//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections.Generic;

namespace blacktriangles.Network
{
  public class ActionConnection
  {
    // events /////////////////////////////////////////////////////////////
    public event Connection.DisconnectCallback OnDisconnect;
    public event Connection.NetworkErrorCallback OnNetworkError;

    // members ////////////////////////////////////////////////////////////
    public BaseConnection connection                            { get; private set; }
    public bool isConnected                                     { get { return connection.isConnectionActive; } }

    private ActionRouter actionRouter                           = null;
    private PacketRouter packetRouter                           = null;
    private List<Packet> outgoingPackets                        = null;

    // constructor / initializer //////////////////////////////////////////
    public ActionConnection()
    {
      connection = null;
      outgoingPackets = new List<Packet>();
      packetRouter = new PacketRouter();
      packetRouter.Initialize();
      packetRouter.AddRoute<JsonPacket>( OnJsonPacket );

      actionRouter = new ActionRouter();
    }

    // public methods /////////////////////////////////////////////////////
    public void Update()
    {
      if( connection != null )
      {
        List<RawPacket> rawPackets = connection.TakePackets();
        foreach( RawPacket packet in rawPackets )
        {
          packetRouter.Route( packet );
        }

        if( outgoingPackets.Count > 0 )
        {
          //System.Console.WriteLine( "Sending: " + outgoingPackets.Count );
          connection.SendPackets( outgoingPackets.ToArray() );
          outgoingPackets.Clear();
        }
      }
    }

    public void Connect( string host, int port )
    {
      if( connection == null )
      {
        SetConnection( new Connection() );
      }

      connection.Connect( host, port );
    }

    public void Connect( BaseConnection conn, string host, int port )
    {
      DebugUtility.Assert( conn != null, "Connect cannot be set with a null connection." );
      SetConnection( conn );
      Connect( host, port );
    }

    public bool Reconnect()
    {
      bool result = false;
      if( connection != null )
      {
        connection.Reconnect();
        result = true;
      }

      return result;
    }

    public void AddRoute<ActionType>( System.Action<BaseConnection,ActionType> handler )
      where ActionType: JsonAction
    {
      actionRouter.AddRoute<ActionType>( handler );
    }

    public void AddCustomRoute<ObjectType>( System.Func<JsonObject,ObjectType> converter, System.Func<BaseConnection,ObjectType,bool> handler )
    {
      actionRouter.AddCustomRoute<ObjectType>( converter, handler );
    }

    public void AddRoute( string action, System.Action<BaseConnection,JsonObject> handler )
    {
      actionRouter.AddRoute( action, handler );
    }

    public void AddPreRoute( System.Func<BaseConnection,JsonObject,bool> handler )
    {
      actionRouter.AddPreRoute( handler );
    }

    public void RemoveRoute( string action, System.Action<BaseConnection,JsonObject> handler )
    {
      actionRouter.RemoveRoute( action, handler );
    }

    public void RemovePreRoute( System.Func<BaseConnection,JsonObject,bool> handler )
    {
      actionRouter.RemovePreRoute( handler );
    }

    public void RemoveCustomRoute<ObjectType>( System.Func<JsonObject,ObjectType> converter, System.Action<BaseConnection,ObjectType> handler )
    {
      actionRouter.RemoveCustomRoute<ObjectType>( converter, handler );
    }

    public void RemoveRoute<ActionType>( System.Action<BaseConnection,ActionType> handler )
      where ActionType: JsonAction
    {
      actionRouter.RemoveRoute<ActionType>( handler );
    }

    public bool Send( IJsonSerializable data )
    {
      bool result = false;
      if( connection != null )
      {
        JsonObject json = data.ToJson();
        outgoingPackets.Add( new JsonPacket( json ) );
        result = true;
      }

      return result;
    }

    public void Simulate( IJsonSerializable data )
    {
      actionRouter.Route( connection, data.ToJson() );
    }

    public void Disconnect()
    {
      if( connection != null )
      {
        connection.Disconnect();
      }

      connection = null;
    }

    // private methods
    private void SetConnection( BaseConnection conn )
    {
      DebugUtility.Assert( conn != null, "SetConnection cannot be set with a null connection." );
      Disconnect();
      connection = conn;
      connection.OnDisconnect += OnDisconnect;
      connection.OnNetworkError += OnNetworkError;
    }

    // callbacks //////////////////////////////////////////////////////////
    private bool OnJsonPacket( BaseConnection connection, JsonPacket packet )
    {
      actionRouter.Route( connection, packet.json );
      return true;
    }
  }
}
