//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Linq;
using System.Collections.Generic;

namespace blacktriangles.Network
{
  public class SimConnection
    : BaseConnection
  {
    // members ////////////////////////////////////////////////////////////////
    private List<Packet> packetsToServer                        = new List<Packet>();
    private List<RawPacket> packetsToClient                     = new List<RawPacket>();

    // public methods /////////////////////////////////////////////////////////
    public override bool isConnectionActive                     { get { return _isConnectionActive; } }
    private bool _isConnectionActive                            = false;

    //// BaseConnection ///////////////////////////////////////////////////////
    public override bool Connect( string hostAddress, int port )
    {
      return Reconnect();
    }

    public override bool Reconnect()
    {
      _isConnectionActive = true;
      lock( packetsToServer )
      {
        packetsToServer = new List<Packet>();
      }

      lock( packetsToClient )
      {
        packetsToClient = new List<RawPacket>();
      }

      return true;
    }

    public override void Disconnect()
    {
      Reconnect();
      _isConnectionActive = false;
    }

    public override void SendPacket( Packet packet )
    {
      if( isConnectionActive == false ) return;
      lock( packetsToServer )
      {
        packetsToServer.Add( packet );
      }
    }

    public override void SendPackets( IEnumerable<Packet> packets )
    {
      if( isConnectionActive == false ) return;
      lock( packetsToServer )
      {
        packetsToServer.AddRange( packets );
      }
    }

    public override List<RawPacket> TakePackets()
    {
      if( isConnectionActive == false ) return new List<RawPacket>();
      List<RawPacket> result = null;
      lock( packetsToClient )
      {
        result = new List<RawPacket>( packetsToClient );
        packetsToClient.Clear();
      }

      return result;
    }

    //// Simulate Methods /////////////////////////////////////////////////////
    public void SendToClient( Packet packet )
    {
      if( isConnectionActive == false ) return;
      RawPacket rawPacket = packet.Encode();
      lock( packetsToClient )
      {
        packetsToClient.Add( rawPacket );
      }
    }

    public void SendToClient( IEnumerable<Packet> packets )
    {
      if( isConnectionActive == false ) return;
      IEnumerable<RawPacket> rawPackets = packets.Select<Packet,RawPacket>(
          (pack)=>{ return pack.Encode(); }
        );

      lock( packetsToClient )
      {
        packetsToClient.AddRange( rawPackets );
      }
    }

    public List<Packet> TakeServerPackets()
    {
      if( isConnectionActive == false ) return new List<Packet>();
      List<Packet> result = null;
      lock( packetsToServer )
      {
        result = new List<Packet>( packetsToServer );
        packetsToServer.Clear();
      }

      return result;
    }
  }
}
