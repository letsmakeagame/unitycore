//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;

namespace blacktriangles.Network
{
  public abstract class BaseConnection
  {
    // events /////////////////////////////////////////////////////////////////
    public delegate void DisconnectCallback( BaseConnection connection );
    public delegate void NetworkErrorCallback( BaseConnection connection, NetworkError error );

    public event DisconnectCallback OnDisconnect;
    public event NetworkErrorCallback OnNetworkError;

    // members ////////////////////////////////////////////////////////////////
    public abstract bool isConnectionActive                     { get; }

    // public methods /////////////////////////////////////////////////////////
    public abstract bool Connect( string hostAddress, int port );
    public abstract bool Reconnect();
    public abstract void Disconnect();

    public abstract void SendPacket( Packet packet );
    public abstract void SendPackets( IEnumerable<Packet> packets );
    public abstract List<RawPacket> TakePackets();

    // protected methods //////////////////////////////////////////////////////
    protected void NotifyDisconnected()
    {
      DisconnectCallback cb = OnDisconnect;
      if( cb != null )
      {
        cb( this );
      }
    }

    protected void NotifyError( string msg )
    {
      NetworkErrorCallback cb = OnNetworkError;
      if( cb != null )
      {
        NetworkError netErr = new NetworkError( this, msg );
        cb( this, netErr );
      }
    }
  }
}
