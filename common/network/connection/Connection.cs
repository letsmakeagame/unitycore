//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Collections.Generic;

namespace blacktriangles.Network
{
  public class Connection
    : BaseConnection
  {
    // constants //////////////////////////////////////////////////////////
    public const int kDefaultUpdatePeriod                       = 10;

    // members ////////////////////////////////////////////////////////////
    public Guid id                                              { get; private set; }
    public int updatePeriod                                     { get { return GetUpdatePeriod(); } set { SetUpdatePeriod( value ); } }
    private int _updatePeriod                                   = kDefaultUpdatePeriod;

    private TcpClient tcpClient                                 = null;
    private Thread connectionThread                             = null;

    private string hostAddress                                  = System.String.Empty;
    private int port                                            = -1;

    private List<RawPacket> incomingPackets                     = new List<RawPacket>();
    private List<Packet> outgoingPackets                        = new List<Packet>();
    private Queue<Packet> processingPackets                     = new Queue<Packet>();

    public override bool isConnectionActive                     { get { return tcpClient != null && tcpClient.Connected && tcpClient.Client.Connected; } }
    public bool canWriteToConnection                            { get { return isConnectionActive; } }
    public bool canReadFromConnection                           { get { return isConnectionActive && tcpClient.Available > 0; } }

    private bool shutdown                                       = false;

    private RawPacketCreator rawPacketCreator                   = new RawPacketCreator();

    // constructor / initializer //////////////////////////////////////////
    public Connection( int _updatePeriod = kDefaultUpdatePeriod )
    {
      id = Guid.NewGuid();
      updatePeriod = _updatePeriod;
    }

    // public methods /////////////////////////////////////////////////////
    public override bool Connect( string _hostAddress, int _port )
    {
      bool result = false;
      hostAddress = _hostAddress;
      port = _port;

      if( isConnectionActive )
      {
        //Debug.LogWarning( "Attempting to connect a connection that is already connected.  Ignoring request." );
        return false;
      }

      try
      {
        tcpClient = new TcpClient();
        tcpClient.Connect( hostAddress, port );

        shutdown = false;
        connectionThread = new Thread( new ThreadStart( this.ClientConnectionLoop ) );
        connectionThread.Start();
        result = true;
      }
      catch( System.Exception ex )
      {
        NotifyError( ex.ToString() );
      }

      return result;
    }

    public override bool Reconnect()
    {
      bool result = false;
      if( hostAddress.Length >= 0 && port >= 0 )
      {
        result = Connect( hostAddress, port );
      }

      return result;
    }

    public override void Disconnect()
    {
      shutdown = true;
      if( connectionThread != null )
      {
        connectionThread.Join( 3000 );
        connectionThread = null;
      }
    }

    public override void SendPacket( Packet packet )
    {
      if( packet == null ) return;

      lock( outgoingPackets )
      {
        outgoingPackets.Add( packet );
      }
    }

    public override void SendPackets( IEnumerable<Packet> packets )
    {
      if( packets == null ) return;

      lock( outgoingPackets )
      {
        outgoingPackets.AddRange( packets );
      }
    }

    public override List<RawPacket> TakePackets()
    {
      List<RawPacket> result = null;
      lock( incomingPackets )
      {
        result = new List<RawPacket>( incomingPackets );
        incomingPackets.Clear();
      }

      return result;
    }

    // private methods ////////////////////////////////////////////////////
    private int GetUpdatePeriod()
    {
      lock( this )
      {
        return _updatePeriod;
      }
    }

    private void SetUpdatePeriod( int value )
    {
      lock( this )
      {
        _updatePeriod = value;
      }
    }

    // threaded loop //////////////////////////////////////////////////////
    private void ClientConnectionLoop()
    {
      NetworkStream netStream = tcpClient.GetStream();
      BinaryWriter binaryWriter = new BinaryWriter( netStream );
      BinaryReader binaryReader = new BinaryReader( netStream );

      while( !shutdown )
      {
        try
        {
          if( tcpClient.Client.Poll( 1, SelectMode.SelectError ) )
          {
            NotifyError( "Connection error encountered." );
          }

          ReadIncomingData( binaryReader );
          WriteOutgoingData( binaryWriter );
        }
        catch( Exception e )
        {
          //Debug.LogError( e.ToString() );
          NotifyError( e.ToString() );
        }

        Thread.Sleep( updatePeriod );
        //Thread.Yield();
      }

      tcpClient.Close();
      netStream.Close();
      binaryWriter.Close();
      binaryReader.Close();

      tcpClient = null;
      NotifyDisconnected();
    }

    // read methods ///////////////////////////////////////////////////////
    private void ReadIncomingData( BinaryReader reader )
    {
      if( canReadFromConnection )
      {
        RawPacket rawPacket = rawPacketCreator.Read( reader );
        if( rawPacket != null )
        {
          lock( incomingPackets )
          {
            rawPacket.source = this;
            incomingPackets.Add( rawPacket );
          }
        }
      }
    }

    // write methods //////////////////////////////////////////////////////
    private void WriteOutgoingData( BinaryWriter writer )
    {
      if( canWriteToConnection )
      {
        if( processingPackets.Count <= 0 )
        {
          if( outgoingPackets.Count > 0 )
          {
            lock( outgoingPackets )
            {
              processingPackets = new Queue<Packet>( outgoingPackets );
              outgoingPackets.Clear();
            }
          }
        }
        else
        {
          Packet sendPacket = processingPackets.Dequeue();
          RawPacket rawPacket = sendPacket.Encode();
          rawPacket.WriteToStream( writer );
        }
      }
    }
  }
}
