//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Based on MiniJSON by Calvin Rien [https://gist.github.com/darktable/1411710]
// which was based on Percurios JSON Parser by Patrick van Bergen [http://techblog.procurios.nl/k/618/news/view/14605/14863/How-do-I-write-my-own-parser-for-JSON.html]
//
// THIS FILE IS LICENSED UNDER MIT LICENSE
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//=============================================================================

using System.IO;
using System.Text;
using System.Collections.Generic;

namespace blacktriangles
{
    public class JsonParser
        : System.IDisposable
    {
        // constants //////////////////////////////////////////////////////////
        private const string kWhitespaceSymbols                 = "\t\n\r ";
        private const string kBreakSymbols                      = "\t\n\r{}[],:\"";

        // types //////////////////////////////////////////////////////////////
        public enum Token
        {
            Colon
          , Comma
          , String
          , Number
          , True
          , False
          , Null
          , CurlyBrace_Open
          , CurlyBrace_Close
          , SquareBrace_Open
          , SquareBrace_Close
          , None
        }

        // members ////////////////////////////////////////////////////////////
        private StringReader reader                             = null;
        private bool isFinished                                 { get { return reader.Peek() == -1; } }

        // constructor / initializer //////////////////////////////////////////
        public JsonParser( string json )
        {
            reader = new StringReader( json );
        }

        public static JsonObject Parse( string json )
        {
            JsonParser parser = new JsonParser( json );
            return parser.ToJsonObject();
        }

        // public methods /////////////////////////////////////////////////////
        public void Dispose()
        {
            reader.Dispose();
            reader = null;
        }

        public JsonObject ToJsonObject()
        {
            return ParseObject();
        }

        public bool IsWhitespace( char c )
        {
            return kWhitespaceSymbols.Contains( c );
        }

        public bool IsWordBreak( char c )
        {
            return kBreakSymbols.Contains( c );
        }

        // parse methods //////////////////////////////////////////////////////
        private object ParseValue()
        {
            Token nextToken = PeekToken();
            return ParseValue( nextToken );
        }

        private object ParseValue( Token token )
        {
            switch( token )
            {
                case Token.CurlyBrace_Open:     return ParseObject();
                case Token.SquareBrace_Open:    return ParseArray();
                case Token.String:              return ParseString();
                case Token.Number:              return ParseNumber();
                case Token.True:                return true;
                case Token.False:               return false;
                case Token.Null:
                default:                        return null;
            }
        }

        private JsonObject ParseObject()
        {
            Dictionary< string, object > fields = new Dictionary< string, object >();

            Token nextToken = PeekToken();
            if( nextToken != Token.CurlyBrace_Open )
                ThrowParseException( "Json Parse Error: Could not find starting brace!" );

            reader.Read();

            JsonObject result = new JsonObject( fields );

            while( !isFinished )
            {
                nextToken = PeekToken();
                switch( nextToken )
                {
                    case Token.None: return null;
                    case Token.Comma: continue;
                    case Token.CurlyBrace_Close: return result;
                    case Token.String:
                        string name = ParseString();
                        if( name == null )
                            ThrowParseException( "Json Parse Error: Could not parse name for field!" );

                        if( PeekToken() != Token.Colon )
                            ThrowParseException( "Json Parse Error: Expecing ':' after parsing name " + name );

                        // eat the colon.
                        reader.Read();

                        fields[ name ] = ParseValue();
                    break;
                    default:
                        ThrowParseException( "Unexpected token while parsing object " + nextToken.ToString() );
                    break;
                }
            }

            return result;
        }

        private object[] ParseArray()
        {
            List<object> result = new List<object>();

            // eat the open brace
            reader.Read();

            bool parsing = true;
            while( parsing && !isFinished )
            {
                Token token = PeekToken();
                switch( token )
                {
                    case Token.None:                ThrowParseException( "Json Parse Error: Unexpected token when attempting to parse array." ); break;
                    case Token.Comma:               continue;
                    case Token.SquareBrace_Close:   parsing = false; break;
                    default:
                        object val = ParseValue( token );
                        if( val != null )
                            result.Add( val );
                    break;
                }
            }

            return result.ToArray();
        }

        private string ParseString()
        {
            // eat the open quote.
            reader.Read();

            StringBuilder result = new StringBuilder();
            bool readingString = true;
            while( readingString && !isFinished )
            {
                char c = reader.ReadChar();
                switch( c )
                {
                    case '"': readingString = false; break;
                    case '\\':
                        if( !isFinished )
                            result.Append( ConvertEscapeChar( reader.ReadChar() ) );
                        break;
                    default: result.Append( c ); break;
                }
            }

            return result.ToString();
        }

        private object ParseNumber()
        {
            object result = null;

            string numAsString = ReadWord();
            if( numAsString.Contains( '.' ) )
            {
                double rational = 0;
                System.Double.TryParse( numAsString, out rational );
                result = rational;
            }
            else
            {
                System.Int64 integer = 0;
                System.Int64.TryParse( numAsString, out integer );
                result = integer;
            }

            return result;
        }

        // private methods ////////////////////////////////////////////////////
        private Token PeekToken()
        {
            SkipWhitespace();

            if( reader.Peek() == -1 )
                return Token.None;

            char c = reader.PeekChar();
            Token result = Token.None;
            switch( c )
            {
                case ':': result = Token.Colon; break;
                case ',': result = Token.Comma; break;
                case '"': result = Token.String; break;
                case '0': result = Token.Number; break;
                case '1': result = Token.Number; break;
                case '2': result = Token.Number; break;
                case '3': result = Token.Number; break;
                case '4': result = Token.Number; break;
                case '5': result = Token.Number; break;
                case '6': result = Token.Number; break;
                case '7': result = Token.Number; break;
                case '8': result = Token.Number; break;
                case '9': result = Token.Number; break;
                case '{': result = Token.CurlyBrace_Open; break;
                case '}': result = Token.CurlyBrace_Close; break;
                case '[': result = Token.SquareBrace_Open; break;
                case ']': result = Token.SquareBrace_Close; break;
                default:
                {
                    string str = ReadWord();
                    switch( str )
                    {
                        case "false":
                        case "FALSE":
                        case "False": result = Token.False; break;
                        case "true":
                        case "TRUE":
                        case "True": result = Token.True; break;
                        case "null": result = Token.Null; break;
                        default: result = Token.None; break;
                    }
                }
                break;
            }

            // for closing braces and comma separator, consume the token
            // we are interested in the next token
            switch( result )
            {
                case Token.CurlyBrace_Close:
                case Token.SquareBrace_Close:
                case Token.Comma:
                    reader.Read();
                break;
            }

            return result;
        }

        private string ReadWord()
        {
            StringBuilder sb = new StringBuilder();
            while( IsWordBreak( reader.PeekChar() ) == false && !isFinished )
            {
                sb.Append( reader.ReadChar() );
            }

            return sb.ToString();
        }

        private void SkipWhitespace()
        {
            while( IsWhitespace( reader.PeekChar() ) && !isFinished )
            {
                reader.Read();
            }
        }

        private char ConvertEscapeChar( char c )
        {
            switch( c )
            {
                case 'b':       return '\b';
                case 'f':       return '\f';
                case 'n':       return '\n';
                case 't':       return '\t';
                case 'r':       return '\r';
                case '"':
                case '\\':
                case '/':
                default:        return c;

            }
        }

        private void ThrowParseException( string message )
        {
            throw new System.ArgumentException( System.String.Format( "[Json Parse Error] {0}", message ) );
        }
    }
}
