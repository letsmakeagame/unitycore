//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;

namespace blacktriangles.FSM
{
	public interface IState
	{
		// members ////////////////////////////////////////////////////////////
		string stateName										{ get; }
		bool isActive											{ get; }

		// public methods /////////////////////////////////////////////////////
		void Initialize( StateMachine stateMachine );
		void UpdateState();
		void EnterState( IState prevState );
		void ExitState();
	}
}
