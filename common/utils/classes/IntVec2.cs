//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
	[System.Serializable]
	public struct IntVec2
		: IJsonSerializable
	{
		// constants //////////////////////////////////////////////////////////////
		public static readonly IntVec2 zero 		= new IntVec2( 0, 0 );
		public static readonly IntVec2 one 			= new IntVec2( 1, 1 );

		public static readonly IntVec2 north 		= new IntVec2( 0, 1 );
		public static readonly IntVec2 east 		= new IntVec2( 1, 0 );
		public static readonly IntVec2 south 		= new IntVec2( 0, -1 );
		public static readonly IntVec2 west 		= new IntVec2( -1, 0 );

		public static readonly IntVec2 northwest 	= north + west;
		public static readonly IntVec2 northeast 	= north + east;
		public static readonly IntVec2 southeast 	= south + east;
		public static readonly IntVec2 southwest 	= south + west;

		// members ////////////////////////////////////////////////////////////////
		public int x;
		public int y;

		public float magnitude									{ get { return GetMagnitude(); } }
		public float sqrMagnitude								{ get { return GetSqrMagnitude(); } }

		// constructors ///////////////////////////////////////////////////////////
		public IntVec2( int _x, int _y )
			: this()
		{
			x = _x;
			y = _y;
		}

		public IntVec2( float _x, float _y )
			: this( (int)_x, (int)_y )
		{
		}

		public IntVec2( IntVec2 copy )
			: this( copy.x, copy.y )
		{
		}

		public IntVec2( Vector2 floatVec2 )
			: this( floatVec2.x, floatVec2.y )
		{
		}

		public static IntVec2 FromVector2Floor( Vector2 vec2 )
		{
			IntVec2 result = new IntVec2();
			result.x = (int)btMath.Floor(vec2.x);
			result.y = (int)btMath.Floor(vec2.y);
			return result;
		}

		public static IntVec2 FromVector2Ceil( Vector2 vec2 )
		{
			IntVec2 result = new IntVec2();
			result.x = (int)btMath.Ceil(vec2.x);
			result.y = (int)btMath.Ceil(vec2.y);
			return result;
		}

		public static IntVec2 FromVector2Round( Vector2 vec2 )
		{
			IntVec2 result = new IntVec2();
			result.x = (int)btMath.Round(vec2.x);
			result.y = (int)btMath.Round(vec2.y);
			return result;
		}

		public static IntVec2 FromVector3Floor( Vector3 vec3 )
		{
			IntVec2 result = new IntVec2();
			result.x = (int)btMath.Floor(vec3.x);
			result.y = (int)btMath.Floor(vec3.y);
			return result;
		}

		public static IntVec2 FromVector3Ceil( Vector3 vec3 )
		{
			IntVec2 result = new IntVec2();
			result.x = (int)btMath.Ceil(vec3.x);
			result.y = (int)btMath.Ceil(vec3.y);
			return result;
		}

		public static IntVec2 FromVector3Round( Vector3 vec3 )
		{
			IntVec2 result = new IntVec2();
			result.x = (int)btMath.Round(vec3.x);
			result.y = (int)btMath.Round(vec3.y);
			return result;
		}

		// public methods /////////////////////////////////////////////////////////
		public override bool Equals( object o )
		{
			IntVec2 test = (IntVec2)o;
			if( o == null ) return false;

			return (test == this);
		}

		public override int GetHashCode()
		{
			return ToString().GetHashCode();
		}

		// operators //////////////////////////////////////////////////////////////
		public override string ToString()
		{
			return System.String.Format( "[{0},{1}]", x, y );
		}

		public Vector2 ToVector2()
		{
			return new Vector2( x, y );
		}

		public Vector3 ToVector3()
		{
			return new Vector3( x, y, 0f );
		}

		public static IntVec2 Min( IntVec2 vec1, IntVec2 vec2 )
		{
			return new IntVec2( vec1.x < vec2.x ? vec1.x : vec2.x, vec1.y < vec2.y ? vec1.y : vec2.y );
		}

		public static IntVec2 Max( IntVec2 vec1, IntVec2 vec2 )
		{
			return new IntVec2( vec1.x > vec2.x ? vec1.x : vec2.x, vec1.y > vec2.y ? vec1.y : vec2.y );
		}

		public static IntRect CreateRect( IntVec2 vec1, IntVec2 vec2 )
		{
			IntVec2 min = Min( vec1, vec2 );
			IntVec2 max = Max( vec1, vec2 );
			return new IntRect( min, max );
		}

		public static bool operator==( IntVec2 lhs, IntVec2 rhs )
		{
			return ( lhs.x == rhs.x && lhs.y == rhs.y );
		}

		public static bool operator!=( IntVec2 lhs, IntVec2 rhs )
		{
			return !( lhs == rhs );
		}

		public static IntVec2 operator+( IntVec2 lhs, IntVec2 rhs )
		{
			return new IntVec2( lhs.x + rhs.x, lhs.y + rhs.y );
		}

		public static IntVec2 operator-( IntVec2 lhs, IntVec2 rhs )
		{
			return new IntVec2( lhs.x - rhs.x, lhs.y - rhs.y );
		}

		public static IntVec2 operator*( IntVec2 lhs, IntVec2 rhs )
		{
			return new IntVec2( lhs.x * rhs.x, lhs.y * rhs.y );
		}

		/// vector3 ////////
		public static Vector2 operator*( IntVec2 lhs, Vector3 rhs )
		{
			return new Vector2( rhs.x * lhs.x, rhs.y * lhs.y );
		}

		public static Vector2 operator*( Vector3 lhs, IntVec2 rhs )
		{
			return rhs * lhs;
		}

		/// int ////////////
		public static IntVec2 operator/( IntVec2 lhs, int rhs )
		{
			return new IntVec2( lhs.x / rhs, lhs.y / rhs );
		}

		public static IntVec2 operator*( IntVec2 lhs, int rhs )
		{
			return new IntVec2( lhs.x * rhs, lhs.y * rhs );
		}

		/// float //////////
		public static Vector2 operator/( IntVec2 lhs, float rhs )
		{
			return new Vector2( lhs.x / rhs, lhs.y / rhs );
		}

		public static Vector2 operator*( IntVec2 lhs, float rhs )
		{
			return new Vector2( lhs.x * rhs, lhs.y * rhs );
		}

		/// json //////////
		public JsonObject ToJson()
		{
			JsonObject result = new JsonObject();
			result["x"] = x;
			result["y"] = y;
			return result;
		}

		public void FromJson( JsonObject json )
		{
			x = json.GetField<int>( "x" );
			y = json.GetField<int>( "y" );
		}

		// private methods ////////////////////////////////////////////////////
		private float GetSqrMagnitude()
		{
			return x*x + y*y;
		}

		private float GetMagnitude()
		{
			return btMath.Sqrt( GetSqrMagnitude() );
		}
	}
}
