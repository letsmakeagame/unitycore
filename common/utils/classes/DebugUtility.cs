//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

namespace blacktriangles
{
    public static class DebugUtility
    {
		public static bool Assert( bool condition, string errorMessage )
		{
			if( !condition ) LogError( errorMessage );
			return condition;
		}

		public static void LogError( string message )
		{
			#if UNITY_ENGINE || UNITY_EDITOR
			 UnityEngine.Debug.LogError( message );
			#else
			System.Console.Error.WriteLine( message );
			#endif
		}

		public static void LogWarning( string message )
		{
			#if UNITY_ENGINE || UNITY_EDITOR
			UnityEngine.Debug.LogWarning( message );
			#else
			System.Console.WriteLine( "WARNING! " + message );
			#endif
		}

		public static void Log( string message )
		{
			#if UNITY_ENGINE || UNITY_EDITOR
			UnityEngine.Debug.Log( message );
			#else
			System.Console.Error.WriteLine( message );
			#endif
		}

    #if UNITY_ENGINE || UNITY_EDITOR
      public static void CreateSphere( string name, UnityEngine.Vector3 pos, UnityEngine.Quaternion rot, float scale )
      {
        UnityEngine.GameObject go = UnityEngine.GameObject.CreatePrimitive( UnityEngine.PrimitiveType.Sphere );
        go.name = name;
        go.transform.position = pos;
        go.transform.localScale = UnityEngine.Vector3.one * scale;
        go.transform.rotation = rot;
        UnityEngine.GameObject.Destroy( go.GetComponent<UnityEngine.Collider>() );
      }
    #endif
	}
}
