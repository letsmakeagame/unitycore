//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;
using MiscUtil;

[System.Serializable]
public class SmoothValue<T>
{
	// members ////////////////////////////////////////////////////////////////
	private Queue<T> values						                            = null;
	private T total									                              = default( T );
	public T smoothed									                            { get { return GetSmoothedValue(); } }
	public int historySize									                      { get; set; }

	// constructor / destructor ///////////////////////////////////////////////
	public SmoothValue( int _historySize )
	{
		historySize = _historySize;
		values = new Queue<T>( historySize );
	}

	// public methods /////////////////////////////////////////////////////////
	public T Add( T value )
	{
		if( values.Count >= historySize )
		{
			T removedValue = values.Dequeue();
			Operator.Subtract( total, removedValue );
		}

		values.Enqueue( value );
		Operator.Add( total, value );
    return GetSmoothedValue();
	}

	public void Set( T value )
	{
		Reset();
		Add( value );
	}

	public void Reset()
	{
		values.Clear();
		total = default( T );
	}

	// private methods ////////////////////////////////////////////////////////
	private T GetSmoothedValue()
	{
		if( values.Count == 0 ) return default( T );

		T result = Operator.Divide( total, (T)System.Convert.ChangeType( values.Count, typeof( T ) ) );
		return result;
	}
}
