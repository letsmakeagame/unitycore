//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class SmoothVector3
{
	// members ////////////////////////////////////////////////////////////////
	private Queue<Vector3> values						                      = null;
	private Vector3 total									                        = Vector3.zero;
	public Vector3 smoothed									                      { get { return GetSmoothedValue(); } }
	public int historySize									                      { get; set; }

	// constructor / destructor ///////////////////////////////////////////////
	public SmoothVector3( int _historySize )
	{
		historySize = _historySize;
		values = new Queue<Vector3>( historySize );
	}

	// public methods /////////////////////////////////////////////////////////
	public Vector3 Add( Vector3 value )
	{
		if( values.Count >= historySize )
		{
			Vector3 removedValue = values.Dequeue();
			total -= removedValue;
		}

		values.Enqueue( value );
		total += value;
    return GetSmoothedValue();
	}

	public void Set( Vector3 value )
	{
		Reset();
		Add( value );
	}

	public void Reset()
	{
		values.Clear();
		total = Vector3.zero;
	}

	// private methods ////////////////////////////////////////////////////////
	private Vector3 GetSmoothedValue()
	{
		if( values.Count == 0 ) return Vector3.zero;

		Vector3 result = total / values.Count;
		result = new Vector3( result.x.IsNearZero() ? 0f : result.x
							, result.y.IsNearZero() ? 0f : result.y
							, result.z.IsNearZero() ? 0f : result.z );
		return result;
	}
}
