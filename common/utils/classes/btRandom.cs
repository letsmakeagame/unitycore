//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Adapted from code originally written by Scott Lilly
// 	http://scottlilly.com/create-better-random-numbers-in-c/
//=============================================================================

using System;
using System.Security.Cryptography;

namespace blacktriangles
{
  public static class btRandom
  {
	  // members ////////////////////////////////////////////////////////////
    private static readonly RNGCryptoServiceProvider _generator = new RNGCryptoServiceProvider();

		// public methods /////////////////////////////////////////////////////
    public static byte RandomByte()
    {
      byte[] randomNumber = new byte[1];
			_generator.GetBytes(randomNumber);
			return randomNumber[0];
    }

    public static double Range( double minimumValue, double maximumValue )
    {
      double asciiValueOfRandomCharacter = (double)RandomByte();
      double multiplier = Math.Max(0, (asciiValueOfRandomCharacter / 255d));
      double range = maximumValue - minimumValue;

      return multiplier * range + minimumValue;
    }

		public static float Range( float minimumValue, float maximumValue )
		{
			double asciiValueOfRandomCharacter = (double)RandomByte();
      float multiplier = (float)Math.Max(0, (asciiValueOfRandomCharacter / 255d));
      float range = maximumValue - minimumValue;

      return multiplier * range + minimumValue;
		}

    public static int Range(int minimumValue, int maximumValue)
    {
      double asciiValueOfRandomCharacter = (double)RandomByte();

      // We are using Math.Max, and substracting 0.00000000001,
      // to ensure "multiplier" will always be between 0.0 and .99999999999
      // Otherwise, it's possible for it to be "1", which causes problems in our rounding.
      double multiplier = Math.Max(0, (asciiValueOfRandomCharacter / 255d) - 0.00000000001d);

      // We need to add one to the range, to allow for the rounding done with Math.Floor
      int range = maximumValue - minimumValue + 1;
      double randomValueInRange = Math.Floor(multiplier * range);

      return (int)(minimumValue + randomValueInRange);
    }
  }
}
