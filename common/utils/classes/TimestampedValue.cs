//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
	public class TimestampedValue<T>
	{
		// members ////////////////////////////////////////////////////////////
		public T val										                            { get { return GetValue(); } set { SetValue( value ); } }
		public float timestamp					                            { get; private set; }
		public float accessTimestamp		                            { get; private set; }
		public float elapsed						                            { get { return Time.time - timestamp; } }
		public float accessElapsed			                            { get { return Time.time - accessTimestamp; } }

		private T _val;

		// constructor / initializer //////////////////////////////////////////
		public TimestampedValue()
		{
			val = default( T );
			timestamp = 0;
			accessTimestamp = 0;
		}

		public TimestampedValue( T defaultValue )
			: this()
		{
			val = defaultValue;
		}

		public TimestampedValue( TimestampedValue<T> copy )
			: this( copy.val )
		{
		}

    // public methods /////////////////////////////////////////////////////////
    public void Restamp()
    {
      timestamp = Time.time;
      accessTimestamp = Time.time;
    }

		// protected methods //////////////////////////////////////////////////////
		protected T GetValue()
		{
			accessTimestamp = Time.time;
			return _val;
		}

		protected void SetValue( T newValue )
		{
			timestamp = Time.time;
			_val = newValue;
		}

		// operators //////////////////////////////////////////////////////////
		public static implicit operator T( TimestampedValue<T> v )
		{
			return v.val;
		}

		public static implicit operator TimestampedValue<T>( T v )
		{
			return new TimestampedValue<T>( v );
		}
	}
}
