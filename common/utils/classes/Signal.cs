//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;

namespace blacktriangles
{
  [Serializable]
  public class Signal< ValueType >
  {
    // events /////////////////////////////////////////////////////////////////
    public delegate void ValueChangedCallback( ValueType oldValue, ValueType newValue );
    public event ValueChangedCallback OnValueChanged;

    // members ////////////////////////////////////////////////////////////////
    public ValueType val                                        { get { return GetValue(); } set { SetValue( value ); } }

    #if UNITY_ENGINE || UNITY_EDITOR
      [UnityEngine.SerializeField]
    #endif
    private ValueType _val                                      = default(ValueType);

    // constructor / destructor ///////////////////////////////////////////////
    public Signal()
    {
      _val = default(ValueType);
    }

    public Signal( ValueType initValue )
    {
      _val = initValue;
    }

    // public methods /////////////////////////////////////////////////////////
    public ValueType GetValue()
    {
      return _val;
    }

    public void SetValue( ValueType newValue )
    {
      if( newValue.Equals( _val ) == false )
      {
        if( OnValueChanged != null )
        {
          OnValueChanged( _val, newValue );
        }

        _val = newValue;
      }
    }

    // operators ////////////////////////////////////////////////////////////
    public static explicit operator ValueType( Signal<ValueType> signal )
    {
      return signal.GetValue();
    }

    public override string ToString()
    {
      return _val.ToString();
    }
  }
}
