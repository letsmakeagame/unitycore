//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

namespace blacktriangles
{
  public class RollingAverage
  {
    // members ////////////////////////////////////////////////////////////////
    public double sum                                           { get; private set; }
    public double average                                       { get { return GetAverage(); } }
    public int maxSize                                          { get; set; }
    public int size                                             { get { return values.Count; } }

    private Deque<double> values                                = new Deque<double>();


    // constructor / destructor ///////////////////////////////////////////////
    public RollingAverage()
    {
      sum = 0;
      maxSize = 0;
    }

    public RollingAverage( int _maxSize )
    {
      sum = 0;
      maxSize = _maxSize;
    }

    // public methods /////////////////////////////////////////////////////////
    public void Add( float value )
    {
      Add( (double)value );
    }

    public void Add( int value )
    {
      Add( (double)value );
    }

    public void Add( double value )
    {
      while( maxSize > 0 && size > maxSize )
      {
        sum -= values.RemoveFromBack();
      }

      sum += value;
      values.AddToFront( value );
    }

    // private methods ////////////////////////////////////////////////////////
    private double GetAverage()
    {
      if( size == 0 ) return 0;
      return sum / size;
    }
  }
}
