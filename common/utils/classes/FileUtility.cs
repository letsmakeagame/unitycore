//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.IO;

namespace blacktriangles
{
    public static class FileUtility
    {
        // utility functions //////////////////////////////////////////////////
        public static void EnsureDirectoryExists( string path )
        {
            if( !System.IO.Directory.Exists( path ) )
                Directory.CreateDirectory( path );
        }

        public static void CopyDirectory( string srcDir, string dstDir )
        {
            if( !System.IO.Directory.Exists( srcDir ) ) return;

            EnsureDirectoryExists( dstDir );

            if( System.IO.Directory.Exists( dstDir ) == false ) DebugUtility.LogError( "Directory " + dstDir + " does not exist!" );

            DirectoryInfo dir = new DirectoryInfo( srcDir );
            DirectoryInfo[] subDirs = dir.GetDirectories();
            FileInfo[] files = dir.GetFiles();

            #if UNITY_EDITOR
                UnityEditor.EditorUtility.DisplayProgressBar( System.String.Format( "Copying {0}", srcDir ), System.String.Empty, 0f );
            #endif

            #if UNITY_EDITOR
                int i = 0;
            #endif
            foreach( FileInfo file in files )
            {
                string path = System.IO.Path.Combine( dstDir, file.Name );
                file.CopyTo( path );
                #if UNITY_EDITOR
                    UnityEditor.EditorUtility.DisplayProgressBar( System.String.Format( "Copying {0}", srcDir ), System.String.Format( "Copying {0}", file.FullName ), ((float)i++/files.Length) );
                #endif
            }

            #if UNITY_EDITOR
                UnityEditor.EditorUtility.ClearProgressBar();
            #endif

            foreach( DirectoryInfo subDir in subDirs )
            {
                CopyDirectory( subDir.FullName, System.IO.Path.Combine( dstDir, subDir.Name ) );
            }
        }

        public static string MakePathRelative( string path, string fromDir )
        {
            System.Uri fullPath = new Uri( path );
            System.Uri relRoot = new Uri( fromDir );
            return System.String.Format( "Assets/{0}", relRoot.MakeRelativeUri( fullPath ).ToString() );
        }

        public static string MakePathRelativeToAssetDir( string fromPath )
        {
            return MakePathRelative( fromPath, System.IO.Path.GetFullPath( "Assets/" ) );
        }
    }
}
