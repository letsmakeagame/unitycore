//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;

namespace blacktriangles
{
	public class BaseRegistry< KeyType, BaseType, AttributeType >
		where BaseType: class
		where AttributeType: System.Attribute
	{
		// members ////////////////////////////////////////////////////////////
		private Assembly assembly								= null;
		private bool isInitialized								= false;
		public Dictionary<KeyType, System.Type> registry		{ get; private set; }

		// constructor / initializer //////////////////////////////////////////
		public BaseRegistry()
		{
		}

		public BaseRegistry( Assembly asm )
		{
			assembly = asm;
		}

		// public methods /////////////////////////////////////////////////////
		public void Initialize( System.Func< AttributeType, KeyType > converter )
		{
			if( !isInitialized )
			{
				registry = new Dictionary<KeyType,System.Type>();

				if( assembly != null )
				{
					ExtractFromAssembly( assembly, converter );
				}
				else
				{
					Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
					foreach( Assembly asm in assemblies )
					{
						ExtractFromAssembly( asm, converter );
					}
				}

				isInitialized = true;
			}
		}

		public BaseType Instantiate( KeyType key, params object[] constructionParameters )
		{
			if( !isInitialized ) return null;

			if( key == null )
			{
				DebugUtility.LogError( "Key is null " + key );
				return null;
			}

			BaseType result = null;
			System.Type resultType = null;
			if( registry.TryGetValue( key, out resultType ) )
			{
				result = System.Activator.CreateInstance( resultType, constructionParameters ) as BaseType;
			}

			return result;
		}

		public AttributeType GetAttributeForKey( KeyType key )
		{
			if( key == null )
			{
				System.Console.Error.WriteLine( "Key is null " + key );
				return null;
			}

			AttributeType attribute = null;
			System.Type type = null;
			if( registry.TryGetValue( key, out type ) )
			{
				attribute = AssemblyUtility.GetAttribute<AttributeType>( type );
			}

			return attribute;
		}

		public System.Type GetSystemTypeForKey( KeyType key )
		{
			if( key == null )
			{
				System.Console.Error.WriteLine( "Key is null " + key );
				return null;
			}

			System.Type result = null;
			registry.TryGetValue( key, out result );
			return result;
		}

		public void PrintRegistered( string header )
		{
			System.Console.WriteLine( header );
			foreach( var item in registry )
			{
				System.Console.WriteLine( "Key: {0}\tValue: {1}", item.Key.ToString(), item.Value.ToString() );
			}
		}

		// private methods ////////////////////////////////////////////////////
		private void ExtractFromAssembly( Assembly asm, System.Func< AttributeType, KeyType > converter )
		{
			System.Type[] typeArray = AssemblyUtility.CollectTypesWithAttribute<AttributeType>( asm );
			foreach( System.Type type in typeArray )
			{
				AttributeType attrib = AssemblyUtility.GetAttribute<AttributeType>( type );
				KeyType key = converter( attrib );
				registry[ key ] = type;
			}
		}
	}
}
