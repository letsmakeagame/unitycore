//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
  public class ReadOnlyDictionary<TKey, TValue>
    : IDictionary<TKey, TValue>
  {
      // members //////////////////////////////////////////////////////////////
      private readonly IDictionary<TKey, TValue> _dictionary;

      // constructor / destructor /////////////////////////////////////////////
      public ReadOnlyDictionary()
      {
          _dictionary = new Dictionary<TKey, TValue>();
      }

      public ReadOnlyDictionary(IDictionary<TKey, TValue> dictionary)
      {
          _dictionary = dictionary;
      }

      // public methods ///////////////////////////////////////////////////////
      public bool ContainsKey(TKey key)
      {
          return _dictionary.ContainsKey(key);
      }

      public ICollection<TKey> Keys
      {
          get { return _dictionary.Keys; }
      }

      public bool TryGetValue(TKey key, out TValue value)
      {
          return _dictionary.TryGetValue(key, out value);
      }

      public ICollection<TValue> Values
      {
          get { return _dictionary.Values; }
      }

      public TValue this[TKey key]
      {
          get
          {
              return _dictionary[key];
          }
      }

      public bool Contains(KeyValuePair<TKey, TValue> item)
      {
          return _dictionary.Contains(item);
      }

      public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
      {
          _dictionary.CopyTo(array, arrayIndex);
      }

      public int Count
      {
          get { return _dictionary.Count; }
      }

      public bool IsReadOnly
      {
          get { return true; }
      }

      public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
      {
          return _dictionary.GetEnumerator();
      }

      // IDictionary methods //////////////////////////////////////////////////
      void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
      {
          throw MakeReadOnlyException();
      }

      bool IDictionary<TKey, TValue>.Remove(TKey key)
      {
          throw MakeReadOnlyException();
      }

      TValue IDictionary<TKey, TValue>.this[TKey key]
      {
          get
          {
              return this[key];
          }
          set
          {
              throw MakeReadOnlyException();
          }
      }

      // ICollection methods //////////////////////////////////////////////////
      void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
      {
          throw MakeReadOnlyException();
      }

      void ICollection<KeyValuePair<TKey, TValue>>.Clear()
      {
          throw MakeReadOnlyException();
      }

      bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
      {
          throw MakeReadOnlyException();
      }

      // IEnumerator methods //////////////////////////////////////////////////
      IEnumerator IEnumerable.GetEnumerator()
      {
          return GetEnumerator();
      }

      // private methdos //////////////////////////////////////////////////////
      private static System.Exception MakeReadOnlyException()
      {
          return new System.NotSupportedException("Attempting to access data in a read only dictionary.");
      }
  }
}
