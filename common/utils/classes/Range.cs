//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using MiscUtil;

namespace blacktriangles
{
  [System.Serializable]
	public class Range<T>
	{
		// members ////////////////////////////////////////////////////////////
		public T min;
		public T max;
		public T length											{ get { return Operator.Subtract( max, min ); } }

		// constructor / initializers /////////////////////////////////////////
		public Range()
			: this( default(T), default(T) )
		{
		}

		public Range( T _min, T _max )
		{
			min = _min;
			max = _max;
		}

    // public methods /////////////////////////////////////////////////////////
    public T Clamp( T val )
    {
      T result = Operator.LessThan( val, min ) ? min : val;
      result = Operator.GreaterThan( result, max ) ? max : result;
      return result;
    }

    public bool IsInRange( T val )
    {
      return Operator.GreaterThan( val, min ) && Operator.LessThan( val, max );
    }
	}

  [System.Serializable]
  public class IntRange
    : Range<int>
  {
    public IntRange( int min, int max )
      : base( min, max )
    {
    }

    public int Random()
    {
      return btRandom.Range( min, max );
    }
  }

  [System.Serializable]
  public class FloatRange
    : Range<float>
  {
    public FloatRange( float min, float max )
      : base( min, max )
    {
    }

    public float Random()
    {
      return btRandom.Range( min, max );
    }
  }
}
