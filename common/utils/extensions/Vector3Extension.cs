//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

public static class Vector3Extension
{
    public static Vector3 Min( Vector3 self, Vector3 other )
    {
        return new Vector3( btMath.Min( self.x, other.x ), btMath.Min( self.y, other.y ), btMath.Min( self.z, other.z ) );
    }

    public static Vector3 Max( Vector3 self, Vector3 other )
    {
        return new Vector3( btMath.Max( self.x, other.x ), btMath.Max( self.y, other.y ), btMath.Max( self.z, other.z ) );
    }

    public static bool IsNearZero( this Vector3 self )
    {
        return self == Vector3.zero;
    }

    public static Vector3 ToVector3XZ( this Vector3 self )
    {
        return self.ToVector3XZ( 0f );
    }

    public static Vector3 ToVector3XZ( this Vector3 self, float y )
    {
        return new Vector3( self.x, y, self.z );
    }

    public static Vector2 ToVector2XY( this Vector3 self )
    {
        return new Vector2( self.x, self.y );
    }

    public static float DistanceSqr( this Vector3 self, Vector3 rhs )
    {
        return ( rhs - self ).sqrMagnitude;
    }

    public static bool IsInRangeSqr( this Vector3 self, Vector3 rhs, float range )
    {
        return self.DistanceSqr( rhs ) < ( range * range );
    }

    public static Vector3 RandomRange( Vector3 min, Vector3 max )
    {
        return new Vector3( btRandom.Range( min.x, max.x ), btRandom.Range( min.y, max.y ), btRandom.Range( min.z, max.z ) );
    }

    public static Vector3 RandomUnitXZ()
    {
        return new Vector3( btRandom.Range( 0f, 1f ), 0f, btRandom.Range( 0f, 1f ) );
    }
}
