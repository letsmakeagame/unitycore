//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//=============================================================================

namespace blacktriangles
{
    public static class ArrayExtension
    {
        public static bool Contains( this System.Array self, object item )
        {
            return self.IndexOf( item ) >= 0;
        }

        public static bool IsValidIndex( this System.Array self, int index )
        {
            return ( index >= 0 && index < self.Length );
        }

        public static int IndexOf( this System.Array self, object item )
        {
            return System.Array.IndexOf( self, item );
        }

        public static int IndexOfFirstNull( this System.Array self )
        {
            for( int i = 0; i < self.Length; ++i )
            {
                if( self.GetValue(i) == null ) return i;
            }

            return -1;
        }

    	public static void Shuffle( this System.Array self )
        {
    		for (int i = self.Length; i > 1; i--)
    		{
    			// Pick random element to swap.
    			int j = btRandom.Range(0,i); // 0 <= j <= i-1
    			// Swap.
    			object tmp = self.GetValue(j);
    			self.SetValue( self.GetValue( i - 1 ), j );
    			self.SetValue( tmp, i - 1 );
    		}
        }

        public static T Random<T>( this T[] self )
        {
            if( self.Length <= 0 ) return default(T);
            return self[ btRandom.Range( 0, self.Length-1 ) ];
        }

        public static void DebugDump( this byte[] self )
        {
            DebugUtility.Log( "ByteArray["+self.Length.ToString()+"]" );
            System.Array.ForEach( self, (b)=>{ DebugUtility.Log( b.ToString() ); } );
        }

        public static T[] Slice<T>(this T[] source, int start, int end)
        {
        	// Handles negative ends.
        	if (end < 0)
        	{
      	    end = source.Length + end;
        	}
        	int len = end - start;

        	// Return new array.
        	T[] res = new T[len];
        	for (int i = 0; i < len; i++)
        	{
      	    res[i] = source[i + start];
        	}
        	return res;
        }
    }
}
