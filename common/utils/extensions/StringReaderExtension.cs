//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.IO;

public static class StringReaderExtension
{
	public static char ReadChar( this StringReader self )
    {
        return System.Convert.ToChar( self.Read() );
    }

    public static char PeekChar( this StringReader self )
    {
        return System.Convert.ToChar( self.Peek() );
    }

    public static bool IsFinished( this StringReader self )
    {
        return (self.Peek() == -1);
    }
}
