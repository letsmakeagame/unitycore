//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
	public static class EditorConfig
	{
		// members ////////////////////////////////////////////////////////////////
		public const string kRootDir						      	            = "Assets/libs/blacktriangles";
		public static readonly string kIconDir					            = System.IO.Path.Combine( kRootDir, "unity3d/utils/editor/art/icons" );
		public static readonly string kTextureDir				            = System.IO.Path.Combine( kRootDir, "unity3d/utils/editor/art/textures" );

		public static Texture2D GetIcon( string name )
		{
			return GetTexture( kIconDir, name );
		}

		public static Texture2D GetTexture( string name )
		{
			return GetTexture( kTextureDir, name );
		}

		private static Texture2D GetTexture( string directory, string fileName )
		{
			string path = System.String.Format( "{0}/{1}.png", directory, fileName );
			Texture2D result = AssetDatabase.LoadAssetAtPath( path, typeof( Texture2D ) ) as Texture2D;

			if( result == null )
			Debug.LogError( "Failed to load Texture at " + path );

			return result;
		}
	}
}
