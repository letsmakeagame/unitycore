//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;

namespace blacktriangles
{
	public static class EditorGUIUtility
	{
		// members ////////////////////////////////////////////////////////////
		private static bool isInitialized						= false;

		// public methods /////////////////////////////////////////////////////
		public static void Initialize()
		{
			GUIStyles.Initialize();
			EditorIcons.LoadIcons();
			isInitialized = true;
		}

		public static void OnGUI()
		{
			if( isInitialized == false )
				Initialize();

			GUIStyles.OnGUI();
		}

		public static T ThinObjectField<T>( string label, T obj, bool allowSceneObjects )
			where T: UnityEngine.Object
		{
			T result = null;
			GUILayout.BeginHorizontal();
				Vector2 size = GUI.skin.label.CalcSize( new GUIContent( label ) );
				GUILayout.Label( label, GUILayout.Width( size.x ) );
				result = EditorGUILayout.ObjectField( obj, typeof( T ), allowSceneObjects ) as T;
			GUILayout.EndHorizontal();

			return result;
		}

		public static object ObjectReflectionGUI( object obj )
		{
			return ObjectReflectionGUI( System.String.Empty, obj );
		}

		public static object ObjectReflectionGUI( string label, object obj )
		{
			return ObjectReflectionGUI( label, obj, null, null );
		}

		public static object ObjectReflectionGUI( string label, object obj, object DEBUG_parent, System.Type _type )
		{
			if( obj == null && _type == null )
			{
				EditorGUILayout.LabelField( label, "[null]" );
				return obj;
			}

			System.Type type = _type ?? obj.GetType();
			if( type == typeof( Bounds ) )
			{
				obj = (object)EditorGUILayout.BoundsField( label, (Bounds)obj );
			}
			else if( type == typeof( Color ) )
			{
				obj = (object)EditorGUILayout.ColorField( label, (Color)obj );
			}
			else if( type == typeof( AnimationCurve ) )
			{
				obj = (object)EditorGUILayout.CurveField( label, (AnimationCurve)obj );
			}
			else if( type.IsEnum )
			{
				obj = (object)EditorGUILayout.EnumPopup( label, (System.Enum)obj );
			}
			else if( type == typeof( float ) || type == typeof( double ) || type == typeof( decimal ) )
			{
				obj = (object)EditorGUILayout.FloatField( label, (float)System.Convert.ToSingle(obj) );
			}
			else if( type == typeof( int ) )
			{
				obj = (object)EditorGUILayout.IntField( label, (int)obj );
			}
			else if( typeof( UnityEngine.Object ).IsAssignableFrom( type ) )
			{
				obj = (object)EditorGUILayout.ObjectField( label, (UnityEngine.Object)obj, typeof( UnityEngine.Object ), true );
			}
			else if( type == typeof( Rect ) )
			{
				obj = (object)EditorGUILayout.RectField( label, (Rect)obj );
			}
			else if( type == typeof( System.String ) )
			{
				obj = (object)EditorGUILayout.TextField( label, (string)obj );
			}
			else if( type == typeof( bool ) )
			{
				obj = (object)EditorGUILayout.Toggle( label, (bool)obj );
			}
			else if( type == typeof( Vector2 ) )
			{
				obj = (object)EditorGUILayout.Vector2Field( label, (Vector2)obj );
			}
			else if( type == typeof( Vector3 ) )
			{
				obj = (object)EditorGUILayout.Vector3Field( label, (Vector3)obj );
			}
			else if( type == typeof( Vector4 ) )
			{
				obj = (object)EditorGUILayout.Vector4Field( label, (Vector4)obj );
			}
			else if( type.IsArray )
			{
				EditorGUILayout.LabelField( System.String.Empty, label );

				EditorGUI.indentLevel += 1;

				System.Array arr = (System.Array)obj;
				if( arr != null )
				{
					for( int i = 0; i < arr.Length; ++i )
					{
						object item = arr.GetValue( i );
						ObjectReflectionGUI( "Item" + i.ToString(), item, obj, item.GetType() );
						arr.SetValue( item, i );
					}

					EditorGUI.indentLevel -= 1;
				}

				obj = (object)arr;
			}
			else
			{
				EditorGUILayout.LabelField( System.String.Empty, label );

				EditorGUI.indentLevel += 1;

				FieldInfo[] fields = type.GetFields();
				foreach( FieldInfo field in fields )
				{
					bool isPublic = field.IsPublic;
					bool isSerializeField = field.GetCustomAttributes( typeof(UnityEngine.SerializeField), true ).Length > 0;
					bool isNonSerialized = field.GetCustomAttributes( typeof(System.NonSerializedAttribute), true ).Length > 0;
					bool isStatic = field.IsStatic;
					if( isStatic == false && isNonSerialized == false && ( isSerializeField || isPublic ) )
					{
						// when using Component references on prefabs, it passes the game object around,
						// so we need to manually GetComponent to get the type we want out of it.
						object original = field.GetValue( obj );
						object result = ObjectReflectionGUI( field.Name, original, obj, field.FieldType );
						if( result != original )
						{
							System.Type resultType = result.GetType();
							System.Type desiredType = field.FieldType;
							if( resultType != desiredType && resultType == typeof(GameObject) )
							{
								GameObject go = (GameObject)result;
								result = go.GetComponent( desiredType );
							}
						}

						field.SetValue( obj, result );
					}
				}

				EditorGUI.indentLevel -=1;
			}

			return obj;
		}

		public static bool Foldout( bool expanded, string label, Texture2D icon )
		{
			const float kSize = 18f;
			EditorGUILayout.BeginHorizontal();
				GUILayout.Label( icon, GUIStyles.buttonNoBorder, GUILayout.Width( kSize ), GUILayout.Height( kSize ) );
				bool result = Foldout( expanded, label );
			EditorGUILayout.EndHorizontal();

			return result;
		}

		public static bool Foldout( bool expanded, string label )
		{
			const float kSize = 18f;

			Texture2D icon = expanded ? EditorIcons.CollapseVertical : EditorIcons.ExpandVertical;
			EditorGUILayout.BeginHorizontal();
				GUILayout.Label( label, GUIStyles.buttonNoBorder );
				if( GUILayout.Button( icon, GUIStyles.buttonNoBorder, GUILayout.Height( kSize ), GUILayout.Width( kSize ) ) )
					expanded = !expanded;
			EditorGUILayout.EndHorizontal();

			return expanded;
		}

		public static bool SerializedObjectField( bool expanded, string label, SerializedObject obj, GUIStyle containerStyle )
		{
			GUILayout.BeginVertical( containerStyle );

				expanded = Foldout( expanded, label );

				if( expanded )
				{
					if( obj == null )
					{
						EditorGUILayout.ObjectField( label, null, typeof( UnityEngine.Object ), false );
					}
					else
					{
						int originalIndent = EditorGUI.indentLevel;
						SerializedProperty prop = obj.GetIterator();
						bool working = prop.NextVisible( true );
						while( working )
						{
							EditorGUI.indentLevel = prop.depth + originalIndent + 1;
							EditorGUILayout.PropertyField( prop );
							working = prop.NextVisible( prop.hasVisibleChildren && prop.isExpanded );
						}

						EditorGUI.indentLevel = originalIndent;
					}
				}

			GUILayout.EndVertical();

			return expanded;
		}

		public static T GetNullClass<T>()
			where T: class
		{
			return null;
		}

		public static void QuickListField<T>( string title, List<T> list, System.Func<T,T> guiCallback, System.Func<T> createCallback, GUIStyle buttonStyle )
		{
			GUILayout.BeginVertical( GUI.skin.box );
				GUILayout.Label( title );
				int deadItem = -1;
				for( int i = 0; i < list.Count; ++i )
				{
					GUILayout.BeginHorizontal();
						list[i] = guiCallback( list[i] );
						if( GUILayout.Button( EditorIcons.X, buttonStyle, GUILayout.Width( 18f ), GUILayout.Height( 18f ) ) )
						{
							deadItem = i;
						}
					GUILayout.EndHorizontal();
				}

				if( deadItem >= 0 )
				{
					list.RemoveAt( deadItem );
				}

				GUILayout.BeginHorizontal();
					if( GUILayout.Button( "Clear" ) )
					{
						list.Clear();
					}

					GUILayout.FlexibleSpace();

					if( createCallback != null )
					{
						if( GUILayout.Button( EditorIcons.Plus, buttonStyle, GUILayout.Width( 18f ), GUILayout.Height( 18f ) ) )
						{
							list.Add( createCallback() );
						}
					}
				GUILayout.EndHorizontal();
			GUILayout.EndVertical();
		}

		// http://answers.unity3d.com/questions/42996/how-to-create-layermask-field-in-a-custom-editorwi.html
		// by FlyingOstriche
		public static LayerMask LayerMaskField( string label, LayerMask layerMask)
		{
			List<string> layers = new List<string>();
			List<int> layerNumbers = new List<int>();

			for (int i = 0; i < 32; i++)
			{
				string layerName = LayerMask.LayerToName(i);
				if (layerName != "")
				{
					layers.Add(layerName);
					layerNumbers.Add(i);
				}
			}

			int maskWithoutEmpty = 0;
			for (int i = 0; i < layerNumbers.Count; i++)
			{
				if (((1 << layerNumbers[i]) & layerMask.value) > 0)
					maskWithoutEmpty |= (1 << i);
			}

			maskWithoutEmpty = EditorGUILayout.MaskField( label, maskWithoutEmpty, layers.ToArray());
			int mask = 0;
			for (int i = 0; i < layerNumbers.Count; i++)
			{
				if ((maskWithoutEmpty & (1 << i)) > 0)
					mask |= (1 << layerNumbers[i]);
			}

			layerMask.value = mask;
			return layerMask;
		}
	}
}
