//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using UnityEditor;

using System.Linq;
using System.Collections.Generic;

namespace blacktriangles
{
	public class SkinnedMeshMergeTool
		: EditorWindow
	{
		// constants //////////////////////////////////////////////////////////
		private const string kMenuPath 							                = "Tools/blacktriangles/Mesh/Skinned Merge Tool";
		private const string kTitle								                  = "Skinned Mesh Merge Tool";

		private List<SkinnedMeshRenderer> meshes				= null;
		private GameObject rigObj								= null;

		// constructor / destructor ///////////////////////////////////////////
		[MenuItem( kMenuPath )]
		private static void OpenWindow()
		{
			SkinnedMeshMergeTool window = GetWindow<SkinnedMeshMergeTool>();
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		private void Initialize()
		{
			meshes = new List<SkinnedMeshRenderer>();
			rigObj = null;
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
			EditorGUIUtility.OnGUI();
			GUILayout.BeginVertical( GUI.skin.box );
				EditorGUIUtility.QuickListField<SkinnedMeshRenderer>( "Meshes", meshes, OnSkinnedMeshGUI, EditorGUIUtility.GetNullClass<SkinnedMeshRenderer>, GUIStyles.buttonNormal );
				if( GUILayout.Button( "Get From Selection" ) )
				{
					meshes.Clear();
					foreach( GameObject selection in Selection.objects )
					{
						if( selection != null )
						{
							meshes.AddRange( selection.GetComponentsInChildren<SkinnedMeshRenderer>() );
						}
					}
				}

				rigObj = EditorGUILayout.ObjectField( "Rig", rigObj, typeof( GameObject ), true ) as GameObject;

				if( GUILayout.Button( "Merge" ) )
				{
					Merge( rigObj, meshes.ToArray() );
				}



			GUILayout.EndVertical();
		}

		// private methods ////////////////////////////////////////////////////
		private SkinnedMeshRenderer OnSkinnedMeshGUI( SkinnedMeshRenderer renderer )
		{
			return EditorGUILayout.ObjectField( renderer, typeof( SkinnedMeshRenderer ), true ) as SkinnedMeshRenderer;
		}

		private void Merge( GameObject rig, SkinnedMeshRenderer[] skins )
		{
			MeshUtility.MergeResult result = MeshUtility.MergeSkinnedMeshes( rig, skins );
			if( result.mesh != null )
			{
				string path = EditorUtility.SaveFilePanelInProject( "Save Merged Mesh", "MergedMesh", "asset", "Save the newly created merged mesh." );
				if( path.Length > 0 )
				{
					Create.Asset( result.mesh, path, false );
					AssetDatabase.Refresh();
				}
			}
		}
	}
}
