//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using UnityEditor;

using System.Linq;
using System.Collections.Generic;

namespace blacktriangles
{
	public class MeshMergeTool
		: EditorWindow
	{
		// constants //////////////////////////////////////////////////////////
		private const string kMenuPath 						= "Tools/blacktriangles/Mesh/Merge Tool";
		private const string kTitle								= "Mesh Merge Tool";

		private List<MeshFilter> meshes							= null;
		private Vector2 scrollPos								= Vector2.zero;

		// constructor / destructor ///////////////////////////////////////////
		[MenuItem( kMenuPath )]
		private static void OpenWindow()
		{
			MeshMergeTool window = GetWindow<MeshMergeTool>();
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		private void Initialize()
		{
			meshes = new List<MeshFilter>();
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
			EditorGUIUtility.OnGUI();
			GUILayout.BeginVertical( GUI.skin.box );
				scrollPos = GUILayout.BeginScrollView( scrollPos );
					EditorGUIUtility.QuickListField<MeshFilter>( "Meshes", meshes, OnMeshGUI, EditorGUIUtility.GetNullClass<MeshFilter>, GUIStyles.buttonNormal );
				GUILayout.EndScrollView();
				if( GUILayout.Button( "Get From Selection" ) )
				{
					meshes.Clear();
					foreach( GameObject selection in Selection.objects )
					{
						if( selection != null )
						{
							meshes.AddRange( selection.GetComponentsInChildren<MeshFilter>() );
						}
					}
				}

				if( GUILayout.Button( "Merge" ) )
				{
					Merge( meshes.ToArray() );
				}

			GUILayout.EndVertical();
		}

		// private methods ////////////////////////////////////////////////////
		private MeshFilter OnMeshGUI( MeshFilter renderer )
		{
			return EditorGUILayout.ObjectField( renderer, typeof( MeshFilter ), true ) as MeshFilter;
		}

		private void Merge( MeshFilter[] meshes )
		{
			Mesh result = MeshUtility.MergeMeshes( meshes );
			if( result != null )
			{
				string path = EditorUtility.SaveFilePanelInProject( "Save Merged Mesh", "MergedMesh", "asset", "Save the newly created merged mesh." );
				if( path.Length > 0 )
				{
					Create.Asset( result, path, false );
					AssetDatabase.Refresh();
				}
			}
		}
	}
}
