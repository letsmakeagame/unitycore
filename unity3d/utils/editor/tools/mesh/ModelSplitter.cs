//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

namespace blacktriangles
{
	public class ModelSplitter
		: EditorWindow
	{
		// constants //////////////////////////////////////////////////////////
		private const string kMenuPath						                	= "Tools/blacktriangles/Mesh/Model Prefab Splitter";
		private const string kTitle								                  = "Model Splitter";

		private GameObject model								= null;
		private GameObject rigRoot								= null;
		private List<GameObject> ignoreDelete					= null;
		private List<GameObject> ignoreKeep						= null;

		// constructor / initializer //////////////////////////////////////////
		[MenuItem( kMenuPath )]
		private static void OpenWindow()
		{
			ModelSplitter window = GetWindow<ModelSplitter>();
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		private void Initialize()
		{
			model = null;
			rigRoot = null;
			ignoreDelete = new List<GameObject>();
			ignoreKeep = new List<GameObject>();
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
			EditorGUIUtility.OnGUI();

			model = EditorGUILayout.ObjectField( "Model To Split", model, typeof( GameObject ), true ) as GameObject;
			if( model == null || PrefabUtility.GetPrefabType( model ) != PrefabType.ModelPrefabInstance )
			{
				EditorGUILayout.HelpBox( "The prefab type must be a Model Instance (instantiated in the scene).", MessageType.Error );
			}
			else
			{
				rigRoot = EditorGUILayout.ObjectField( "Rig Root", rigRoot, typeof( GameObject ), true ) as GameObject;
				System.Func<GameObject,GameObject> guiCallback = (go)=>{ return EditorGUILayout.ObjectField( go, typeof(GameObject), true) as GameObject; };
				EditorGUIUtility.QuickListField<GameObject>( "Ignore Delete", ignoreDelete, guiCallback, EditorGUIUtility.GetNullClass<GameObject>, GUIStyles.buttonNormal );
				EditorGUIUtility.QuickListField<GameObject>( "Ignore Keep", ignoreKeep, guiCallback, EditorGUIUtility.GetNullClass<GameObject>, GUIStyles.buttonNormal );

				float count = 0f;

				List<GameObject> splitObjects = new List<GameObject>();
				if( GUILayout.Button( "Split" ) )
				{
					GameObject split = new GameObject();
					split.name = "SPLIT";

					List<GameObject> destroy = new List<GameObject>();
					foreach( Transform child in model.transform )
					{
						EditorUtility.DisplayProgressBar( "Splitting Model", child.name, count++/(float)model.transform.childCount );
						if( child.gameObject != rigRoot )
						{
							// clone the original
							GameObject newGo = GameObject.Instantiate( model, Vector3.zero, Quaternion.identity ) as GameObject;
							newGo.name = child.name;
							newGo.transform.SetParent( split.transform );
							splitObjects.Add( newGo );

							// mark objects for destroy
							foreach( Transform newChild in newGo.transform )
							{
								if( newChild.name != rigRoot.name && newChild.name != newGo.name )
								{
									destroy.Add( newChild.gameObject );
								}
							}
						}
					}

					// destroy objects
					for( int i = 0; i < destroy.Count; ++i )
					{
						GameObject.DestroyImmediate( destroy[i] );
					}

					destroy = null;

					EditorUtility.ClearProgressBar();
				}
			}
		}
	}
}
