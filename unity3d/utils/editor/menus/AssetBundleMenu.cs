//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using System;
using System.IO;

namespace blacktriangles
{
    public static class AssetBundleMenu
    {
		// constants //////////////////////////////////////////////////////////
		public static readonly string kAssetBundleFolderName	= "assetbundles";

		// public methods /////////////////////////////////////////////////////
		[MenuItem("Assets/Build AssetBundles")]
        [MenuItem("Tools/blacktriangles/Common/Build AssetBundles")]
		static void BuildAllAssetBundles()
		{
			System.IO.Directory.CreateDirectory( "assetbundles" );
			BuildPipeline.BuildAssetBundles( kAssetBundleFolderName );
		}
	}
}
