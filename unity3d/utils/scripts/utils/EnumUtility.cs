//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
	public static class EnumUtility
	{
    public static int Count<EnumType>()
    {
      return System.Enum.GetValues( typeof( EnumType ) ).Length;
    }
  }
}
