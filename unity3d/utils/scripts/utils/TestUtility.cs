//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

namespace blacktriangles.Test
{
    public static class TestUtility
    {
        // members ////////////////////////////////////////////////////////////
        private static string currentTestName                   = System.String.Empty;
        private static int testsRun                             = 0;
        private static int failed                               = 0;

        // public methods /////////////////////////////////////////////////////
        public static void BeginTest( string name )
        {
            currentTestName = name;
            testsRun = 0;
            failed = 0;
        }

        public static int Assert( bool condition )
        {
            if( !condition )
            {
                UnityEngine.Debug.LogError( "ASSERT FAILED!" );
                ++failed;
            }

            return ++testsRun;
        }

        public static void EndTest()
        {
            string message = System.String.Format( "Completed {0}: \tRun: {1} | Failed: {2}", currentTestName, testsRun, failed );
            if( failed > 0 )
                UnityEngine.Debug.LogWarning( message );
            else
                UnityEngine.Debug.Log( message );
        }
    }
}

#endif
