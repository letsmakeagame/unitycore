//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace blacktriangles.Test
{
    public abstract class TestSuite
		: MonoBehaviour
	{
		// types //////////////////////////////////////////////////////////////
		public struct Test
		{
			public string name;
			public System.Func<IEnumerator> method;
            public System.Action action;
			public Test( string _name, System.Func<IEnumerator> _method )
			{
				name = _name;
				method = _method;
                action = null;
			}

            public Test( string _name, System.Action _action )
            {
                name = _name;
                method = null;
                action = _action;
            }
		}

		// members ////////////////////////////////////////////////////////////
		private List<Test> tests								= new List<Test>();
		private static int testsRun								= 0;
		private static int testsFailed							= 0;

		// protected methods //////////////////////////////////////////////////
        protected virtual void AddTest( string name, System.Action method )
        {
            Test newTest = new Test( name, method );
            tests.Add( newTest );
        }

        protected virtual void AddCoroutineTest( string name, System.Func<IEnumerator> method )
        {
            Test newTest = new Test( name, method );
            tests.Add( newTest );
        }

        protected virtual void StartTests()
        {
            StartTests( (run,failed)=>
                {
                    Debug.Log( System.String.Format( "Run: {0} | Failed: {1}", run, failed ) );
                }
            );
        }

		protected virtual void StartTests( System.Action<int,int> onComplete )
		{
			StartCoroutine( StartTestCoroutine( onComplete ) );
		}

		protected virtual IEnumerator StartTestCoroutine( System.Action<int,int> onComplete )
		{
			foreach( Test test in tests )
			{
				Debug.Log( test.name );
                if( test.method != null )
                {
				    yield return StartCoroutine( test.method() );
                }
                else
                {
                    test.action();
                }
			}

			onComplete( testsRun, testsFailed );
		}

		public virtual void Assert( bool condition )
		{
			if( !condition )
			{
				++testsFailed;
				Debug.LogError( "ASSERT" );
			}

			++testsRun;
		}
	}
}

#endif
