//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections;
using System.Collections.Generic;

#if FALSE

namespace blacktriangles
{
  public class OrderedList<ItemType>
    : IList<ItemType>
    , ICollection<ItemType>
    , IList
    , ICollection
    , IEnumerable<ItemType>
    , IEnumerable
    where ComparerType: IComparer<ItemType>
  {
    // members ////////////////////////////////////////////////////////////////
    private List<ItemType> list                                 = null;
    private IComparer<ItemType> comparer                        = null;

    public int Capacity                                         { get { return list.Capacity; } set { list.Capacity = value; } }
    public int Count                                            { get { return list.Count; } }

    ICollection.IsSynchronized                                  { get { return false; } }
    object ICollection.SyncRoot                                 { get { return list; } }
    bool IList.IsFixedSize                                      { get { return false; } }
    bool IList.IsReadOnly                                       { get { return false; } }
    bool ICollection<ItemType>.IsReadOnly                       { get { return false; } }

    object IList.this[int index]                                { get { return list[index]; } set { list[index] = value; } }
    public ItemType this[int index]                             { get { return list[index]; } set { list[index] = value; } }

    // constructors / initializers ////////////////////////////////////////////
    public OrderedList()
      : list( new List<ItemType>() )
      , comparer( ComparerType.Default )
    {
    }

    public OrderedList( IComparer<ItemType> _comparer )
      : list( new List<ItemType>() )
      , comparer( _comparer )
    {
    }

    // public methods /////////////////////////////////////////////////////////
    void ICollection<ItemType>.Add( ItemType item )
    {
      Add( item );
    }

    int IList.Add( object item )
    {
      return Add( (ItemType)item );
    }

    public int Add( ItemType item )
    {

    }

    public void AddRange( IEnumerable<ItemType> collection )
    {
    }

    public int BinarySearch( ItemType item )
    {
    }

    public int BinarySearch( int index, int count, ItemType item )
    {
    }

    public ReadOnlyCollection<ItemType> AsReadOnly()
    {
      return list.AsReadOnly();
    }

    public void Clear()
    {
      return list.Clear();
    }

    public bool Contains( ItemType item )
    {
      return BinarySearch( item ) >= 0;
    }

    bool IList.Contains( object item )
    {
      return Contains( (ItemType)item );
    }

    public List<TOutput> ConvertAll<TOutput>( Converter<T, TOutput> converter )
    {
      return list.ConvertAll( converter );
    }

    public void CopyTo( ItemType[] array )
    {
      list.CopyTo( array );
    }

    public void CopyTo( ItemType[] array, int arrayIndex )
    {
      list.CopyTo( array, arrayIndex );
    }

    void ICollection.CopyTo( Array array, int arrayIndex )
    {
      list.CopyTo( (T[])array, arrayIndex );
    }

    public void CopyTo( int index, ItemType[] array, int arrayIndex, int count )
    {
      list.CopyTo( index, array, arrayIndex, count );
    }

    public List<ItemType> GetRange( int index, int count )
    {
      return list.GetRange( index, count );
    }

    public bool Remove( ItemType item )
    {
      bool result = false;
      int index = BinarySearch( item );
      if( index >= 0 )
      {
        result = true;
        list.RemoveAt( index );
      }

      return result;
    }

    void IList.Remove( object item )
    {
      Remove( (ItemType)item );
    }

    public void RemoveAt( int index )
    {
      list.RemoveAt(index);
    }

    public void RemoveRange(int index, int count)
    {
      list.RemoveRange(index, count);
    }

    public T[] ToArray()
    {
      return list.ToArray();
    }

    public void TrimExcess()
    {
      list.TrimExcess();
    }

    public int IndexOf(T item)
    {
        int index = BinarySearch(item);
        if (index < 0) return -1;
        while(_list[--index].Equals(item)){}
        return ++index;
    }

    int IList.IndexOf(object item)
    {
        return IndexOf((T)item);
    }

    // not implemented ////////////////////////////////////////////////////////
    void IList.Insert(int index, object item)
    {
        throw new NotImplementedException(InsertExceptionMsg);
    }

    void IList<T>.Insert(int index, T item)
    {
        throw new NotImplementedException(InsertExceptionMsg);
    }

    // operators //////////////////////////////////////////////////////////////
    public void ForEach( Action<ItemType action )
    {
      foreach( ItemType item in list )
      {
        action( item );
      }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return list.GetEnumerator();
    }

    public IEnumerator<ItemType> GetEnumerator()
    {
      return list.GetEnumerator();
    }
  }
}

#endif
