//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections;

namespace blacktriangles
{
	public class DestroyOnCollide
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		public bool onTriggers									= false;
		public LayerMask ignoreLayers							= default(LayerMask);

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnCollisionEnter( Collision collision )
		{
			CheckDestroy( collision.gameObject );
		}

		protected virtual void OnTriggerEnter( Collider collider )
		{
			if( onTriggers )
			{
				CheckDestroy( collider.gameObject );
			}
		}

		protected virtual void OnCollisionEner2D( Collision2D collision )
		{
			CheckDestroy( collision.gameObject );
		}

		protected virtual void OnTriggerEnter2D( Collider2D collider )
		{
			if( onTriggers )
			{
				CheckDestroy( collider.gameObject );
			}
		}

		// private methods ////////////////////////////////////////////////////
		private void CheckDestroy( GameObject other )
		{
			if( ((1 << other.layer) & ignoreLayers.value ) == 0 )
			{
				Destroy( gameObject );
			}
		}
	}
}
