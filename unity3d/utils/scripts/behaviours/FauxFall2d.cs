//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections;

namespace blacktriangles
{
	public class FauxFall2d
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		public float startingHeight								= 1f;
		public AnimationCurve fallCurve							= new AnimationCurve();
		public bool destroyOnLand								= false;
		public SpriteRenderer shadowPrefab						= null;
		private SpriteRenderer shadow							= null;
		public Rigidbody2D rigidbody2d							= null;

		public float height										{ get; private set; }
		private float elapsed									= 0f;

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void Awake()
		{
			height = startingHeight;
			shadow = Instantiate( shadowPrefab, transform.position, Quaternion.identity ) as SpriteRenderer;
		}

		protected virtual void OnDestroy()
		{
			shadow = null;
		}

		protected virtual void FixedUpdate()
		{
			elapsed += Time.fixedDeltaTime;
			float fallDelta = fallCurve.Evaluate(elapsed) * Time.fixedDeltaTime;
			height -= fallDelta;

			Vector2 velocity = rigidbody2d.velocity;
			rigidbody2d.velocity = new Vector2( velocity.x, velocity.y-fallDelta );
			shadow.transform.position = rigidbody2d.position - new Vector2( 0f, height );
		}
	}
}
