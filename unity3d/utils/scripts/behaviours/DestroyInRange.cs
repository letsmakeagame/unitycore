//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections;

namespace blacktriangles
{
	public class DestroyInRange
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////
		public float range										= 1f;
		public Vector3 targetPos								{ get { return targetTrans == null ? _targetPos : targetTrans.position; } }
		public Transform targetTrans							= null;
		[SerializeField] private Vector3 _targetPos				= Vector3.zero;

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void Update()
		{
			if( (targetPos - transform.position).sqrMagnitude <= range*range )
			{
				Destroy( gameObject );
			}
		}
	}
}
