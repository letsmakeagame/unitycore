//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

public static class MonoBehaviourExtension
{
    public static void Invoke( this MonoBehaviour self, System.Action func, float delayInSeconds )
    {
        self.StartCoroutine( DelayedCall( func, delayInSeconds ) );
    }

    private static System.Collections.IEnumerator DelayedCall( System.Action func, float delayInSeconds )
    {
        yield return new WaitForSeconds( delayInSeconds );
        func();
    }
}
