//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
	public abstract class BaseGameManager
		: MonoBehaviour
	{
    // types //////////////////////////////////////////////////////////////////
    public struct DelayedCallback
    {
      public System.Action cb;
      public float endTime;
    }

		// members ////////////////////////////////////////////////////////////////
		public static BaseGameManager instance						          { get; protected set; }

		public Canvas gameCanvas								                    = null;

    private Deque<DelayedCallback> delayedCallbacks              = new Deque<DelayedCallback>();

    // public methods /////////////////////////////////////////////////////////
    public void AddDelayedCallback( System.Action cb, float duration )
    {
      DelayedCallback dcb = new DelayedCallback();
      dcb.cb = cb;
      dcb.endTime = Time.time + duration;
      delayedCallbacks.InsertSorted( dcb, (lhs,rhs)=>{ return lhs.endTime < rhs.endTime; } );
    }

		// unity callbacks ////////////////////////////////////////////////////////
		protected virtual void Awake()
		{
			instance = this;
		}

    protected virtual void Update()
    {
      while( delayedCallbacks.Count > 0 && delayedCallbacks[0].endTime < Time.time )
      {
        delayedCallbacks[0].cb();
        delayedCallbacks.RemoveFromFront();
      }
    }

    // utility methods ////////////////////////////////////////////////////////
    protected static GameManagerType EnsureExists<GameManagerType>( string prefabPath )
      where GameManagerType: BaseGameManager
    {
    	if( instance == null )
			{
				GameObject prefab = Resources.Load( prefabPath, typeof( GameObject ) ) as GameObject;
				GameObject go = Instantiate( prefab );
				instance = go.GetComponent<GameManagerType>();
			}

			return instance as GameManagerType;
    }
	}
}
