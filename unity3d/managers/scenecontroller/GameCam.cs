//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles
{
	public class GameCam
		: MonoBehaviour
	{
    // members ////////////////////////////////////////////////////////////////
    public Camera unityCamera                                   = null;
  }
}
