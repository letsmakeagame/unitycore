//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles
{
	public interface ICameraController
	{
    // members ////////////////////////////////////////////////////////////////
    bool hasCamera                                              { get; }

    // methods ////////////////////////////////////////////////////////////////
    void OnTakeCamera( btCamera camera );
    void OnReleaseCamera();
  }
}
