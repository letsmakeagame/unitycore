//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles
{
	public abstract class btCamera
    : MonoBehaviour
	{
    // members ////////////////////////////////////////////////////////////////
    public Camera unityCamera                                   = null;
    public float speed                                          = 10.0f;
    public float distanceModifier                               = 10.0f;
    public float maxSpeed                                       = 100.0f;
    public float minMoveDist                                    = 2f;
    public float minMoveDist2                                   { get { return minMoveDist * minMoveDist; } }

    // public methods /////////////////////////////////////////////////////////
    public void Move( Vector3 delta )
    {
      float mag = delta.magnitude;
      if( mag < minMoveDist ) return;

      float moveDist = speed * Time.deltaTime;
      if( distanceModifier > 0.0f )
      {
        moveDist *= Mathf.Min( maxSpeed, Mathf.Max( 1.0f, mag / distanceModifier ) );
      }


      transform.position += delta.normalized * Mathf.Min( mag, moveDist );
    }


    //// utilities ////////////////////////////////////////////////////////////
    public bool ScreenToWorld( Vector3 screenPos, out Vector3 result )
    {
      return ScreenToWorld( screenPos, out result, Vector3.up, Vector3.zero );
    }

    public Vector3 WorldToScreenPoint( Vector3 worldPos )
    {
      return unityCamera.WorldToScreenPoint( worldPos );
    }

    public bool ScreenToWorld( Vector3 screenPos
                                  , out Vector3 result
                                  , Vector3 inNormal
                                  , Vector3 inPoint
      )
		{
      result = Vector3.zero;

      bool success = false;
			if( unityCamera != null )
      {

  			Ray ray = unityCamera.ScreenPointToRay( screenPos );
  			Plane groundPlane = new Plane( Vector3.up, Vector3.zero );

  			float distance = 0f;
  			groundPlane.Raycast( ray, out distance );

  			result = ray.GetPoint( distance );
        success = true;
      }

      return success;
		}

    public bool ScreenToWorld( Vector3 screenPos, LayerMask mask, out Vector3 result )
    {
      result = Vector3.zero;

      bool success = false;
      if( unityCamera != null )
      {
        Ray ray = unityCamera.ScreenPointToRay( screenPos );

        RaycastHit hit;
        if( Physics.Raycast( ray.origin, ray.direction, out hit, Mathf.Infinity, mask ) )
        {
          success = true;
          result = hit.point;
        }
      }

      return success;
    }
  }
}
