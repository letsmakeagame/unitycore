//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using blacktriangles;

namespace blacktriangles
{
	public abstract class BaseSceneController
		: MonoBehaviour
	{
		// members ////////////////////////////////////////////////////////////////
		public btCamera sceneCam						                        = null;
		public Canvas screenCanvas				                          = null;
		public Canvas worldCanvas					                          = null;
		public EventSystem eventSystem		                          = null;

    private ICameraController cameraController                  = null;

    // public methods /////////////////////////////////////////////////////////
    public void RequestCamera( ICameraController newController )
    {
      if( cameraController != null )
        cameraController.OnReleaseCamera();

      newController.OnTakeCamera( sceneCam );
      cameraController = newController;
    }

    public void ReleaseCamera( ICameraController oldController )
    {
      if( cameraController == oldController )
      {
        cameraController.OnReleaseCamera();
        cameraController = null;
      }
    }
	}
}
