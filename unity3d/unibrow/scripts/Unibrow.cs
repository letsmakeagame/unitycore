//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com

//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// A mutant combination of Mustache, Handlebars.js and our own custom blend of
// templating deliciousness.
//=============================================================================

using System.IO;
using System.Text;
using System.Collections.Generic;

using UnityEngine;

namespace blacktriangles
{
    public static class Unibrow
    {
        // public methods /////////////////////////////////////////////////////
        public static string Render( string template, JsonObject data )
        {
            if( System.String.IsNullOrEmpty( template ) ) return System.String.Empty;

            StringReader reader = new StringReader( template );
            StringBuilder rendered = new StringBuilder();
            while( !reader.IsFinished() )
            {
                string token = default(string);
                string leadingString = ReadToToken( out token, reader );

                rendered.Append( leadingString );
                if( token.StartsWith( "#foreach" ) )
                {
                    string[] statement = token.Split( ' ' );
                    string openToken = statement[0];
                    string valueName = statement[1];

                    string block = ReadBlock( openToken, reader );
                    object val = ResolveValue( valueName, data );
                    if( val != null && val.GetType().IsArray )
                    {
                        object[] values = (object[])ResolveValue( valueName, data );
                        for( int i = 0; i < values.Length; ++i )
                        {
                            JsonObject blockData = JsonObject.Clone( data );
                            blockData["#parent"] = data["#value"];
                            blockData["#value"] = values[i];
                            blockData["#index"] = i;
                            rendered.Append( Render( block, blockData ) );
                        }
                    }
                }
                else
                {
                    rendered.Append( ResolveValue( token, data ).ToString() );
                }
            }

            return rendered.ToString();
        }

        // private types //////////////////////////////////////////////////////

        // private methods ////////////////////////////////////////////////////
        private static string ReadBlock( string openToken, StringReader reader )
        {
            StringBuilder result = new StringBuilder();

            string closeToken = '/' + openToken.Substring(1);
            int openCount = 1;

            while( openCount > 0 && !reader.IsFinished() )
            {
                string token = default(string);
                string leadingString = ReadToToken( out token, reader );
                result.Append( leadingString );

                if( token.Length > 0 )
                {
                    if( token == closeToken )
                    {
                        --openCount;
                    }
                    else if( token.StartsWith( openToken ) )
                    {
                        ++openCount;
                    }

                    result.Append( "{{" + token + "}}" );
                }
            }

            return result.ToString();
        }

        private static string ReadToToken( out string token, StringReader reader )
        {
            string result = ReadToPair( '{', '{', reader );
            token = ReadToPair( '}', '}', reader );
            return result;
        }

        private static string ReadToPair( char a, char b, StringReader reader )
        {
            StringBuilder builder = new StringBuilder();

            bool foundPattern = false;
            while( !reader.IsFinished() && !foundPattern )
            {
                char c = reader.ReadChar();
                if( c == a && reader.PeekChar() == b )
                {
                    // discard the second character in the pattern
                    reader.Read();
                    foundPattern = true;
                }
                else
                {
                    builder.Append( c );
                }
            }

            return builder.ToString();
        }

        private static object ResolveValue( string token, JsonObject data )
        {
            object result = System.String.Empty;
            if( data != null )
            {
                if( token.Contains( '.' ) == false )
                {
                    object field = data[ token ];
                    if( field != null )
                        result = field;
                }
                else
                {
                    string[] path = token.Split( '.' );
                    if( path.Length > 0 )
                    {
                        JsonObject json = data;
                        for( int i = 0; i < path.Length; ++i )
                        {
                            string sub = path[i];
                            object field = json[ sub ];
                            if( field != null )
                            {
                                if( field.GetType() == typeof( JsonObject ) )
                                {
                                    json = (JsonObject)field;
                                }
                                else if( i == path.Length-1 )
                                {
                                    result = field;
                                }
                            }
                            else
                            {
                                Debug.LogError( System.String.Format( "Failed to resolve path '{0}' at {1}", token, sub ) );
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}
