//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles.Input
{
	public class InputKeyMap<ActionTypeEnum>
		: IJsonSerializable
	{
		// members ////////////////////////////////////////////////////////////////
		public Dictionary<ActionTypeEnum, List<InputAction> > actions = new Dictionary<ActionTypeEnum, List<InputAction> >();

		// public methods /////////////////////////////////////////////////////////
		public void Bind( ActionTypeEnum type, InputAction action )
		{
			List<InputAction> actionList = null;
			if( actions.TryGetValue( type, out actionList ) == false )
			{
				actionList = new List<InputAction>();
				actions[ type ] = actionList;
			}

			actionList.Add( action );
		}

		public void Unbind( ActionTypeEnum type, InputAction action )
		{
			List<InputAction> actionList = null;
			if( actions.TryGetValue( type, out actionList ) )
			{
				actionList.RemoveAll( (x) => { return x.keycode == action.keycode || x.axis == action.axis; } );
			}
		}

		// IJsonSerializable //////////////////////////////////////////////////////
		public JsonObject ToJson()
		{
			JsonObject result = new JsonObject();
			result["actions"] = actions;
			return result;
		}

		public void FromJson( JsonObject json )
		{
			JsonObject actionJson = json.GetField<JsonObject>("actions");
			ActionTypeEnum[] actionTypes = (ActionTypeEnum[])System.Enum.GetValues( typeof( ActionTypeEnum ) );
			foreach( ActionTypeEnum actionType in actionTypes )
			{
				List<InputAction> actionList = new List<InputAction>();
				InputAction[] actionArray = actionJson.GetField<InputAction[]>( actionType.ToString() );
				if( actionArray != null )
				{
					actionList.AddRange( actionArray );
					actions[ actionType ] = actionList;
				}
			}
		}
	}
}
