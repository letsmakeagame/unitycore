//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace blacktriangles.Input
{
	public class InputAction
		: IJsonSerializable
	{
		// types //////////////////////////////////////////////////////////////////
		public enum Axis
		{
			None
			, MouseHorizontal
			, MouseVertical
			, MouseWheel
			, JoystickAxis1
			, JoystickAxis2
			, JoystickAxis3
			, JoystickAxis4
			, JoystickAxis5
			, JoystickAxis6
			, JoystickAxis7
			, JoystickAxis8
			, JoystickAxis9
			, JoystickAxis10
		};

		private static readonly string[] kAxisToString =
		{
			"None"
			, "Mouse Horizontal"
			, "Mouse Vertical"
			, "Mouse Wheel"
			, "Joy1 Axis 1"
			, "Joy1 Axis 2"
			, "Joy1 Axis 3"
			, "Joy1 Axis 4"
			, "Joy1 Axis 5"
			, "Joy1 Axis 6"
			, "Joy1 Axis 7"
			, "Joy1 Axis 8"
			, "Joy1 Axis 9"
			, "Joy1 Axis 10"
		};

		public enum Type { Key, Axis, Combo };

		// members ////////////////////////////////////////////////////////////
		public KeyCode keycode									                    = KeyCode.None;
		public KeyCode negativeCode							                    = KeyCode.None;
		public KeyCode comboKey									                    = KeyCode.None;
		public Axis axis										                        = Axis.None;
		public string axisName									                    { get { return kAxisToString[(int)axis]; } }
		public bool invertAxis									                    = false;
		public float sensitivity								                    = 1f;

		public Type type										                        { get; private set; }

		// constructor / initializer //////////////////////////////////////////
		public static InputAction FromKey( KeyCode code )
		{
			InputAction result = new InputAction();
			result.type = Type.Key;
			result.keycode = code;
			return result;
		}

		public static InputAction FromAxis( Axis axis, bool invert, float sensitivity )
		{
			InputAction result = new InputAction();
			result.type = Type.Axis;
			result.keycode = KeyCode.None;
			result.axis = axis;
			result.invertAxis = invert;
			result.sensitivity = sensitivity;
			return result;
		}

		public static InputAction FromAxis( KeyCode minus, KeyCode plus )
		{
			InputAction result = new InputAction();
			result.type = Type.Axis;
			result.keycode = plus;
			result.negativeCode = minus;
			return result;
		}

		public static InputAction FromCombo( KeyCode key1, KeyCode key2 )
		{
			InputAction result = new InputAction();
			result.type = Type.Combo;
			result.keycode = key1;
			result.comboKey = key2;
			return result;
		}

		private InputAction()
		{
		}

		// IJsonSerializable //////////////////////////////////////////////////
		public JsonObject ToJson()
		{
			JsonObject result = new JsonObject();
			result["keycode"] = keycode;
			result["negativeCode"] = negativeCode;
			result["axis"] = axis;
			result["invertAxis"] = invertAxis;
			result["sensitivity"] = sensitivity;
			return result;
		}

		public void FromJson( JsonObject json )
		{
			keycode = json.GetField<KeyCode>("keycode");
			negativeCode = json.GetField<KeyCode>("negativeCode");
			axis = json.GetField<Axis>("axis");
			invertAxis = json.GetField<bool>("invertAxis");
			sensitivity = json.GetField<float>("sensitivity");
		}
	};
}
