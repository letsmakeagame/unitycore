//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Input
{
	public interface IInputRouter
	{
		// members ////////////////////////////////////////////////////////////
		System.Type actionType									{ get; }

		// methods ////////////////////////////////////////////////////////////
		List<InputAction> GetActions( System.Enum type );
		float GetAxis( System.Enum type );
		bool GetKeyDown( System.Enum type );
		bool GetKey( System.Enum type );
		bool GetKeyUp( System.Enum type );
	}
}
