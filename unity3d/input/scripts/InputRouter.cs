//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Input
{
	public class InputRouter<ActionTypeEnum>
		: IInputRouter
		, IJsonSerializable
	{
		// members ////////////////////////////////////////////////////////////////
		public System.Type actionType							                  { get { return typeof(ActionTypeEnum); } }
		public InputKeyMap<ActionTypeEnum>                          activeMapping		= null;

    public Vector2 mousePosition                                { get { return UnityEngine.Input.mousePosition; } }

		// constructor / initializer //////////////////////////////////////////
		public InputRouter()
		{
			activeMapping = new InputKeyMap<ActionTypeEnum>();
	 	}

		// public methods /////////////////////////////////////////////////////////
		public List<InputAction> GetActions( ActionTypeEnum type )
		{
			List<InputAction> actions = null;
			activeMapping.actions.TryGetValue( type, out actions );
			DebugUtility.Assert( actions != null, "No action has been assigned to " + type.ToString() );
			return actions;
		}

		public float GetAxis( ActionTypeEnum type )
		{
			float result = 0f;
			List<InputAction> actions = GetActions( type );
			foreach( InputAction action in actions )
			{
				if( action.type == InputAction.Type.Axis )
				{
					result += GetAxisState( action );
				}
			}

			return result;
		}

		public float GetAxisState( InputAction action )
		{
			float result = 0f;
			if( action.type == InputAction.Type.Axis )
			{
				if( action.axis != InputAction.Axis.None )
				{
					result = UnityEngine.Input.GetAxis( action.axisName ) * ( action.invertAxis ? -1f : 1f ) * action.sensitivity;
				}
				else if( action.negativeCode != KeyCode.None )
				{
					float positive = UnityEngine.Input.GetKey( action.keycode ) ? 1f : 0f;
					float negative = UnityEngine.Input.GetKey( action.negativeCode ) ? 1f : 0f;
					result = positive - negative;
				}
			}

			return result;
		}

		public bool GetKeyDown( ActionTypeEnum type )
		{
			bool result = false;
			List<InputAction> actions = GetActions( type );

			foreach( InputAction action in actions )
			{
				if( action.type == InputAction.Type.Key )
				{
					result = result || UnityEngine.Input.GetKeyDown( action.keycode );
				}
				else if( action.type == InputAction.Type.Combo )
				{
					// either key was pressed this frame, and the other key is being pressed.
					bool didComboCompleteThisFrame = ( UnityEngine.Input.GetKeyDown( action.keycode ) && UnityEngine.Input.GetKey( action.comboKey ) )
													|| ( UnityEngine.Input.GetKey( action.keycode ) && UnityEngine.Input.GetKeyDown( action.comboKey ) );

					result = result || didComboCompleteThisFrame;
				}

				if( result ) break;
			}

			return result;
		}

		public bool GetKey( ActionTypeEnum type )
		{
			bool result = false;
			List<InputAction> actions = GetActions( type );

			foreach( InputAction action in actions )
			{
				if( action.type == InputAction.Type.Key )
				{
					result = result || UnityEngine.Input.GetKey( action.keycode );
				}
				else if( action.type == InputAction.Type.Combo )
				{
					result = result || (UnityEngine.Input.GetKey( action.keycode ) && UnityEngine.Input.GetKey( action.comboKey ));
				}

				if( result ) break;
			}

			return result;
		}

		public bool GetKeyUp( ActionTypeEnum type )
		{
			bool result = false;
			List<InputAction> actions = GetActions( type );

			foreach( InputAction action in actions )
			{
				if( action.type == InputAction.Type.Key )
				{
					result = result || UnityEngine.Input.GetKeyUp( action.keycode );
				}

				if( result ) break;
			}

			return result;
		}

		// IInputManager //////////////////////////////////////////////////////
		public List<InputAction> GetActions( System.Enum type )
		{
			DebugUtility.Assert( type.GetType() == typeof(ActionTypeEnum), "Attempting to use an action type that is incompatible with this InputManager" );
			return GetActions( (ActionTypeEnum)((object)type) );
		}

		public float GetAxis( System.Enum type )
		{
			DebugUtility.Assert( type.GetType() == typeof(ActionTypeEnum), "Attempting to use an action type that is incompatible with this InputManager" );
			return GetAxis( (ActionTypeEnum)((object)type) );
		}

		public bool GetKeyDown( System.Enum type )
		{
			DebugUtility.Assert( type.GetType() == typeof(ActionTypeEnum), "Attempting to use an action type that is incompatible with this InputManager" );
			return GetKeyDown( (ActionTypeEnum)((object)type) );
		}

		public bool GetKey( System.Enum type )
		{
			DebugUtility.Assert( type.GetType() == typeof(ActionTypeEnum), "Attempting to use an action type that is incompatible with this InputManager" );
			return GetKey( (ActionTypeEnum)((object)type) );
		}

		public bool GetKeyUp( System.Enum type )
		{
			DebugUtility.Assert( type.GetType() == typeof(ActionTypeEnum), "Attempting to use an action type that is incompatible with this InputManager" );
			return GetKeyUp( (ActionTypeEnum)((object)type) );
		}

		// IJsonSerializable //////////////////////////////////////////////////
		public JsonObject ToJson()
		{
			JsonObject result = new JsonObject();
			result["mapping"] = activeMapping.ToJson();
			return result;
		}

		public void FromJson( JsonObject json )
		{
			JsonObject.SafeDeserialize( activeMapping, json, "mapping" );
		}
	}
}
