//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using blacktriangles;

namespace blacktriangles.Input
{
  public static class ToolsMenu
  {
  	[MenuItem( "Tools/blacktriangles/Input/Setup Project Input Setings" )]
		public static void SetupProjectInputSettings()
		{
			string src = System.IO.Path.GetFullPath( System.IO.Path.Combine( EditorConfig.kRootDir, "unity3d/input/data/ProjectSettingsOverride.yaml" ) );
			DebugUtility.Assert( System.IO.File.Exists( src ), "Cannot find " + src + " did you setup the kRootDir in EditorConfig?" );
			string dst = System.IO.Path.GetFullPath( "ProjectSettings/InputManager.asset" );
			DebugUtility.Assert( System.IO.File.Exists( dst ), "Could not find " + dst + " ... I don't know why this would ever happen honestly." );

			System.IO.File.Copy( dst, dst+".bak", true );
			System.IO.File.Copy( src, dst, true );
		}
  }
}
