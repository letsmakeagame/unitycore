//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
	public static class Config
	{
		// members ////////////////////////////////////////////////////////////
		public static JsonObject data							= null;

		// constructor / initializer //////////////////////////////////////////
		public static void Initialize( AssetBundle configFiles )
		{
			if( configFiles != null )
			{
				data = new JsonObject();
				TextAsset[] assets = configFiles.LoadAllAssets<TextAsset>();
				foreach( TextAsset asset in assets )
				{
					data[ asset.name ] = JsonObject.FromString( asset.text );
				}
			}
		}
	}
}
