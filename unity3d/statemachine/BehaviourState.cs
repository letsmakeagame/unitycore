//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles.FSM
{
	public abstract class BehaviourState
		: MonoBehaviour, IState
	{
		// members ////////////////////////////////////////////////////////////
		public string stateName									{ get { return name; } }
		public StateMachine stateMachine						{ get; protected set; }
		public virtual bool isActive							{ get { return _isActive; } }
		private bool _isActive									= false;

		// constructor / initializer //////////////////////////////////////////
		public virtual void Initialize( StateMachine _stateMachine )
		{
			stateMachine = _stateMachine;
		}

		// public methods /////////////////////////////////////////////////////
		public void UpdateState()
		{
			OnUpdate();
		}

		public void EnterState( IState prevState )
		{
			_isActive = true;
			OnEnter( prevState );
		}

		public void ExitState()
		{
			_isActive = false;
			OnExit();
		}

		public abstract void OnUpdate();
		public abstract void OnEnter( IState prevState );
		public abstract void OnExit();
	}
}
